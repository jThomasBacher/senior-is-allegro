//
//  gunEnemy.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 1/13/18.
//  Copyright © 2018 Tommy Bacher. All rights reserved.
//

#include "gunEnemy.hpp"

gunEnemy::gunEnemy(int x, int y, int h, int w, defaultColors c, int hp, std::string n, int sS, int ran, int s, int sD, int dmg, player *target) : enemy(x, y, h, w, c, hp), primaryWeapon(n, sS, ran, s, sD, dmg), targetLoc(0, 0){
    targetLoc.setX( (rand() % int(viewWidth * 29.0 / 30)) + viewWidth / 30);
    targetLoc.setY( (rand() % int(viewHeight * 29.0 / 30)) + viewHeight / 30);
    
    timeUntilMove = 4.0 / GAME_TIME_INTERVAL;
    startTime = al_get_timer_count(gameTimer);
}
gunEnemy::gunEnemy(vertex start, vertex e, defaultColors c,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg, player *target) : enemy(start, e, c, hp), primaryWeapon(n, sS, ran, s, sD, dmg), targetLoc(0, 0){
    targetLoc.setX( (rand() % int(viewWidth * 29.0 / 30)) + viewWidth / 30);
    targetLoc.setY( (rand() % int(viewHeight * 29.0 / 30)) + viewHeight / 30);
    
    timeUntilMove = 4.0 / GAME_TIME_INTERVAL;
    startTime = al_get_timer_count(gameTimer);
}
gunEnemy::gunEnemy(int x, int y, int h, int w, int r, int g, int b,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg, player *target) : enemy(x, y, h, w, r, g, b, hp), primaryWeapon(n, sS, ran, s, sD, dmg), targetLoc(0, 0){
    targetLoc.setX( (rand() % int(viewWidth * 29.0 / 30)) + viewWidth / 30);
    targetLoc.setY( (rand() % int(viewHeight * 29.0 / 30)) + viewHeight / 30);
    
    timeUntilMove = 4.0 / GAME_TIME_INTERVAL;
    startTime = al_get_timer_count(gameTimer);
}
gunEnemy::gunEnemy(vertex start, vertex e, int r, int g, int b,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg, player *target) : enemy(start, e, r, g, b, hp), primaryWeapon(n, sS, ran, s, sD, dmg), targetLoc(0, 0){
    targetLoc.setX( (rand() % int(viewWidth * 29.0 / 30)) + viewWidth / 30);
    targetLoc.setY( (rand() % int(viewHeight * 29.0 / 30)) + viewHeight / 30);
    
    timeUntilMove = 4.0 / GAME_TIME_INTERVAL;
    startTime = al_get_timer_count(gameTimer);
}

void gunEnemy::behavior(player *target){
    //Always try to stay inline with player
    
    primaryWeapon.fire(this->getMe(), target->getMe().getCenter() , 200, 140, 50);
    
    int toRemove = -10;
    primaryWeapon.update();
    bool collide;
    for (int i = 0; i < primaryWeapon.getShots().size(); i++) {
        collide = entityCollison(primaryWeapon.getShots()[i].getMe(), target->getMe());
        if(collide){
            if(!target->isInvincible()){
                target->adjustHealth(-primaryWeapon.getDmg());
                target->nowInvincible();
                target->takeDamage(primaryWeapon.getDmg());
                toRemove = i;
            }
            else{
                toRemove = i;
            }
        }
    }
    if (toRemove != -10)
        primaryWeapon.deleteProjectile(toRemove);
    
    
    if ((getCenterX() == targetLoc.getX() && getCenterY() == targetLoc.getY()) || al_get_timer_count(gameTimer) > startTime + timeUntilMove ) {
        
        //Locations need to be relative to the size of the enenmy, so it should go from
        //Lower bound of 0 + width (so just width) to max - width
        //and y is height to max - height
        int xLowerBound = getMe().getWidth(), yLowerBound = getMe().getHeight();
        int xUpperBound = viewWidth - xLowerBound, yUpperBound = viewHeight - yLowerBound;
        
        targetLoc.setX( (rand() % (xUpperBound - xLowerBound)) + xLowerBound);
        targetLoc.setY( (rand() % (yUpperBound - yLowerBound)) + yLowerBound);
        
        
        startTime = al_get_timer_count(gameTimer);
        timeUntilMove = rand() % int(1.5 / GAME_TIME_INTERVAL) + int(1.5 / GAME_TIME_INTERVAL);
    }
    
    direction d1 = Undefined, d2;
    bool notLeftRight = false;
    if(targetLoc.getX() - getSpeed() - 1 > getCenterX())
        d1 = Right;
    else if(targetLoc.getX() + getSpeed() + 1 < getCenterX())
        d1 = Left;
    else
        notLeftRight = true;
    
    
    
    if (targetLoc.getY() - getSpeed() - 1 > getCenterY())
        d2 = Up;
    else if(targetLoc.getY() + getSpeed() + 1 < getCenterY())
        d2 = Down;
    else
        d2 = d1;
    
    if (notLeftRight) {
        d1 = d2;
    }
    
    move(d1);
    move(d2);
}

void gunEnemy::draw(){
    enemy::draw();
    for (int i = 0; i < primaryWeapon.getShots().size(); i++) {
        primaryWeapon.getShots()[i].draw();
    }
}

void gunEnemy::chooseNewLoc(player *target){
    int dirc = rand() % 4;
    dirc = 2;
    
    if(dirc == 0){
        targetLoc.setX(target->getCenterX());
        targetLoc.setY(target->getCenterY() + rand() % (primaryWeapon.getRange() - 15) + 15);
        if(targetLoc.getY() > viewHeight - getMe().getHeight())
            targetLoc.setY(viewHeight - getMe().getHeight());
        
        firingDirection = Down;
    }
    else if(dirc == 1){
        targetLoc.setX(target->getCenterX());
        targetLoc.setY(target->getCenterY() - rand() % (primaryWeapon.getRange() - 15) + 15);
        if(targetLoc.getY() < getMe().getHeight())
            targetLoc.setY(getMe().getHeight());
        
        firingDirection = Up;
    }
    else if(dirc == 2){
        targetLoc.setX(target->getCenterX() + rand() % (primaryWeapon.getRange() - 15) + 15);
        targetLoc.setY(target->getCenterY());
        if(targetLoc.getX() > viewWidth - getMe().getWidth())
            targetLoc.setX(viewWidth - getMe().getWidth());
        
        firingDirection = Left;
    }
    else if(dirc == 3){
        targetLoc.setX(target->getCenterX() - rand() % (primaryWeapon.getRange() - 15) + 15);
        targetLoc.setY(target->getCenterY());
        if(targetLoc.getX() < getMe().getWidth())
            targetLoc.setX(getMe().getWidth());
        
        firingDirection = Right;
    }
}
