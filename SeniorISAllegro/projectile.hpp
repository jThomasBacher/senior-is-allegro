//
//  projectile.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 11/2/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef projectile_hpp
#define projectile_hpp

#include "entity.hpp"

class projectile {
    entity me;
    int speed;
    int distTraveled;
    
    int rise, run;
    float riseTrav = 0, runTrav = 0;
    
    float riseOverRun;
    bool left, up; // Bools noting if the projectile is going
public:
    projectile(int x, int y, int h, int w, defaultColors c, int rise, int run, int sp, bool l, bool rt);
    projectile(vertex s, vertex e, defaultColors c, int rise, int run, int sp, bool l, bool rt);
    projectile(int x, int y, int h, int w, int r, int g, int b, int rise, int run, int sp, bool l, bool rt);
    projectile(vertex s, vertex e, int r, int g, int b, int rise, int run, int sp, bool l, bool rt);
    
    entity getMe(){return me;}
    
    void update();
    
    int getDist(){return distTraveled;}
    void changeDist(int delta){distTraveled += delta;}
    
    bool riseIsLarger(){return rise > run;}
    
    void draw();
};

#endif /* projectile_hpp */
