//
//  chasingEnemy.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 1/13/18.
//  Copyright © 2018 Tommy Bacher. All rights reserved.
//

#include "chasingEnemy.hpp"

chasingEnemy::chasingEnemy(int x, int y, int h, int w, defaultColors c, int hp) : enemy(x, y, h, w, c, hp){}
chasingEnemy::chasingEnemy(vertex s, vertex e, defaultColors c, int hp) : enemy(s, e, c, hp){}
chasingEnemy::chasingEnemy(int x, int y, int h, int w, int r, int g, int b, int hp) : enemy(x, y, h, w, r, g, b, hp) {}
chasingEnemy::chasingEnemy(vertex s, vertex e, int r, int g, int b, int hp) : enemy(s, e, r, g, b, hp) {}

void chasingEnemy::draw(){
    enemy::draw();
}

void chasingEnemy::behavior(player *target){
    /*
        Moving options: Up Right, Up Left, Down Right, Down Left, Down Stationary, Up, Stationary Left, Stationary Right, Stationary Stationary
     */
    
    if(target->getCenterX() - getSize() / 2 > getCenterX())
        move(Right);
    else if(target->getCenterX() + getSize() / 2 < getCenterX())
        move(Left);
    
    
    
    if (target->getCenterY() - getSize() / 2 > getCenterY())
        move(Up);
    else if(target->getCenterY() + getSize() / 2 < getCenterY())
        move(Down);
    
}
