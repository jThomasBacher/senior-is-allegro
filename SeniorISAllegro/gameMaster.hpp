//
//  gameMaster.hpp
//  seniorIS
//
//  Created by John Bacher on 9/26/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef gameMaster_hpp
#define gameMaster_hpp


#include <vector>
#include "defines.h"
#include "boss.hpp"
#include "chasingEnemy.hpp"
#include "gunEnemy.hpp"
#include "room.hpp"
#include "weapon.hpp"
#include "powerUp.hpp"
#include "data.hpp"

class gameMaster {
    player playerOne;
	powerUp *generalUp;
    std::vector<std::shared_ptr<enemy>> enemies;
    std::vector<room> floor;
    std::vector<weapon> weapons;
    int weaponEquipped;
    
    ALLEGRO_DISPLAY *window;
    ALLEGRO_EVENT_QUEUE *eventQueue;
    ALLEGRO_FONT *gameFont;
    
    data testData;
    data errorLog;

	bool roomFinished;
    
    int nextEmptyRoom; //Integer corrospondidsng to the next rooms position in the vector
    int currRoom; //The Room the player is currently in
    
    int numberOfKeys;
    //Key State Array
    bool *key;
    
    bool bossSpawned = false;
    
    /* Dynamic Adjustment Variables */
    int enemyPointTemplate;
    int roomsWithoutDamage;
    int totalRoomsFinished;
    int pointsToPowerUp = 400;
    int roomsToHealth = 4;
    
    int enemyBaseHealth;
    
    int enemiesDefeated, enemiesToSpawnBoss;
    int pointsDefeated, pointsToSpawnBoss;

	bool powerUpSpawned = false, powerUpAdjust = false;
    
    
public:
    gameMaster(ALLEGRO_DISPLAY *d);
    ~gameMaster();
    
    //Returns true if win, false if lose
    bool play();
    
    void display();
    
    void finishRoom();
    
    void resetPlayerPosition();

	/*Given the difficulties I've had with room generation, and how unimportant it is to the whole
		purpose of this project, I'm going to switch to a new system:
		When you finish a room, it flags a variable called roomFinished = true
		and you have to hit *spacebar* which deletes everything left and then calls generate Enemies*/
	void generateNewRoom(); 
    
    //Returns the index of the room at index (x, y) otherwise returns -1
    int roomAtIndex(int xP, int yP);
    
    bool playerContainedWithinRoom(player p, room r);
    bool playerContainedWithinEntity(player p, entity e);
    bool entityCollison(entity one, entity two);
    //Returns the door the player collides with
    int checkDoors(player p);
    
    void updateEnemies();
    void generateEnemies();

	void resetOpenGLMatrices();

	void applyPowerUp(powerUp plus);
	void updatePowerUps();
	void spawnPowerUp(powerUpType t, int s);

	bool dynamicAdjust();
    
    int generateEnemyX(int enemySize);
    int generateEnemyY(int enemySize);
    
    vertex generateEnemyLoc(int enemySize);
    
    void updatePlayerData();
    void updateEnemyPointTemplate();
    void addEnemyData(std::shared_ptr<enemy> e, int pointVal, int type);
    

    
    
  /*  void initPlay();
    void play();
    void playIdle();
    void playKeyboard(unsigned char key, int x, int y);
    void playKeyboardUp(unsigned char key, int x, int y);
    
    void generateLevel();
    void generateNewRoom();
    bool playerCollideWithRoom(player p, room r);*/
    
};

#endif /* gameMaster_hpp */
