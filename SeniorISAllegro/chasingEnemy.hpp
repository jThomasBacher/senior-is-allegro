//
//  chasingEnemy.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 1/13/18.
//  Copyright © 2018 Tommy Bacher. All rights reserved.
//

#ifndef chasingEnemy_hpp
#define chasingEnemy_hpp

#include "enemy.hpp"

class chasingEnemy : public enemy {
    
    
public:
    chasingEnemy(int x, int y, int h, int w, defaultColors c, int hp);
    chasingEnemy(vertex s, vertex e, defaultColors c, int hp);
    chasingEnemy(int x, int y, int h, int w, int r, int g, int b, int hp);
    chasingEnemy(vertex s, vertex e, int r, int g, int b, int hp);
    
    void draw();
    
    void behavior(player* target);
};

#endif /* chasingEnemy_hpp */
