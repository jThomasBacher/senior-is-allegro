

#ifndef powerUp_hpp
#define powerUp_hpp

#include "entity.hpp"

enum powerUpType{Damage, Range, ShotSpeed, FireRate, Health, ShotSize, MovementSpeed};

class powerUp
{
	entity me;

	powerUpType type;
	int strength;

public:
	powerUp(powerUpType t, int s, int x, int y, int w, int h);

	//PowerUps will be stars so as to be visually distinct, but their hitboxes are nonetheless squares because it's all irrelevant
	void drawPowerUp();
	
	entity getMe() { return me; }

	powerUpType getType() { return type; }
	void setType(powerUpType t) { type = t; }

	int getStrength() { return strength; }
};

#endif

