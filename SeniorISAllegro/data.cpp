//
//  data.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 1/28/18.
//  Copyright © 2018 Tommy Bacher. All rights reserved.
//

#include "data.hpp"

data::data(){
    currentIndex = 0;
    stream.push_back("");
    delim = "DDA Test ";
}
data::data(std::string d){
    currentIndex = 0;
    stream.push_back("");
    delim = d;
}

void data::pushBack(std::string s){
    if (stream[currentIndex].size() + s.size() < stream[currentIndex].max_size()) {
        stream[currentIndex].append(s);
    }
    else{
        stream.push_back(s);
    }
}

bool data::saveToFile(){
    //Need to make
    std::time_t curr = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    
    std::string fileName = delim;
    fileName.append(std::ctime(&curr));
    
    fileName = fileName.substr(0, fileName.length() - 1);
    
    std::ofstream output;
    output.open(fileName);
    
    for (int i = 0; i < stream.size(); i++) {
        output << stream[i];
    }
    
    output.close();
    return true;
}
