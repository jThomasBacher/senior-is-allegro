//
//  player.cpp
//  seniorIS
//
//  Created by John Bacher on 9/26/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "player.hpp"

player::player(int x, int y, int h, int w, defaultColors c) : me(x, y, h, w, c){
    speed = 4 * speedFactor;
    health = 8;
    invincible = false;
    damageTaken = 0;
}
player::player(vertex s, vertex e, defaultColors c) : me(s, e, c){
    speed = 4 * speedFactor;
    health = 8;
    invincible = false;
    damageTaken = 0;
}
player::player(int x, int y, int h, int w, int r, int g, int b) : me(x, y, h, w, r, g, b){
    speed = 4 * speedFactor;
    health = 8;
    invincible = false;
    damageTaken = 0;

}
player::player(vertex s, vertex e, int r, int g, int b) : me(s, e, r, g, b){
    speed = 4 * speedFactor;
    health = 8;
    invincible = false;
    damageTaken = 0;
}

void player::drawPlayer(){
    
    me.drawEntity();
    if (invincible) {
        if(al_get_timer_count(gameTimer) > startTime + invinTimer){
            removeInvincible();
            me.setColor(Green);
        }
        unsigned long store = al_get_timer_count(gameTimer);
        if(store - startTime < 0.25 / GAME_TIME_INTERVAL){
            me.setColor(255, 255, 0);
        }
        else if(store - startTime < 0.5 / GAME_TIME_INTERVAL){
            me.setColor(DarkGreen);
        }
        else if(store - startTime < 0.75 / GAME_TIME_INTERVAL){
            me.setColor(255, 255, 0);
        }
        else if(store - startTime < 1.0 / GAME_TIME_INTERVAL){
            me.setColor(DarkGreen);
        }
        else if(store - startTime < 1.25 / GAME_TIME_INTERVAL){
            me.setColor(255, 255, 0);
        }
        else if(store - startTime < 1.5 / GAME_TIME_INTERVAL){
            me.setColor(DarkGreen);
        }
        else if(store - startTime < 1.75 / GAME_TIME_INTERVAL){
            me.setColor(255, 255, 0);
        }
        else if(store - startTime < 2.0 / GAME_TIME_INTERVAL){
            me.setColor(DarkGreen);
        }
        else if(store - startTime < 2.25 / GAME_TIME_INTERVAL){
            me.setColor(255, 255, 0);
        }
        else if(store - startTime < 2.5 / GAME_TIME_INTERVAL){
            me.setColor(DarkGreen);
        }
        else if(store - startTime < 2.75 / GAME_TIME_INTERVAL){
            me.setColor(255, 255, 0);
        }
    
    }
}

void player::drawHealth(){
    if(isInvincible())
        glColor3ub(255, 255, 0);
    else
        glColor3ub(255, 0, 0);
    glPushMatrix();
    glTranslatef(30, -40, 0);
    for (int i = 0;  i < health; i++) {
        glBegin(GL_POLYGON);
        glVertex2i(10, viewHeight - 10);
        glVertex2i(50, viewHeight - 25);
        glVertex2i(50, viewHeight - 60);
        glEnd();
        glBegin(GL_POLYGON);
        glVertex2i(90, viewHeight - 10);
        glVertex2i(50, viewHeight - 60);
        glVertex2i(50, viewHeight - 25);
        glEnd();
        glTranslatef(85, 0, 0);
    }
    glPopMatrix();
}

void player::move(direction d){
    switch (d) {
        case Up:
            me.up(speed);
            break;
        case Left:
            me.left(speed);
            break;
        case Right:
            me.right(speed);
            break;
        default:
            me.down(speed);
            break;
    }
}

void player::resetVerts(int x, int y, int w, int h){
    me.resetVerts(x, y, w, h);
}
void player::resetVerts(vertex s, vertex e){
    me.resetVerts(s, e);
}

void player::takeDamage(int i){
    damageTaken += i;
    if(i > 0)
        totalDamageTaken += i;
}
