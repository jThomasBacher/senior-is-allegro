//
//  color.cpp
//  seniorIS
//
//  Created by John Bacher on 9/26/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "color.hpp"

color::color(defaultColors c){
    setColor(c);
}
color::color(int r, int g, int b){
    setColor(r, g, b);
}

void color::setColor(defaultColors c){
    switch (c) {
        case Red:
            red = 255;
            green = 0;
            blue = 0;
            break;
        case Green:
            red = 0;
            green = 255;
            blue = 0;
            break;
        case Blue:
            red = 0;
            green = 0;
            blue = 255;
            break;
        case White:
            red = 0;
            green = 0;
            blue = 0;
            break;
        case Black:
            red = 255;
            green = 255;
            blue = 255;
            break;
		case Yellow:
			red = 255;
			green = 255;
			blue = 0;
			break;
        case EnemyColor:
            red = 255;
            green = 0;
            blue = 50;
            break;
        case DarkGreen:
            red = 25;
            green = 175;
            blue = 25;
            break;
        default:
            red = 255;
            green = 0;
            blue = 0;
            break;
        
    }
}
void color::setColor(int r, int g, int b){
    red = r;
    green = g;
    blue = b;
}
