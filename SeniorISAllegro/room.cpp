//
//  room.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 10/4/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "room.hpp"

room::room(int x, int y, int w, int h, defaultColors c, int xP, int yP) : me(x, y, w, h, c){
    leftExit = -1;
    rightExit = -1;
    topExit = -1;
    bottomExit = -1;
    
    xPos = xP;
    yPos = yP;
    
    complete = false;
}
room::room(vertex s, vertex e, defaultColors c, int xP, int yP) : me(s, e, c){
    leftExit = -1;
    rightExit = -1;
    topExit = -1;
    bottomExit = -1;
    
    xPos = xP;
    yPos = yP;
    
    complete = false;
}
room::room(int x, int y, int w, int h, int r, int g, int b, int xP, int yP) : me(x, y, w, h, r, g, b){
    leftExit = -1;
    rightExit = -1;
    topExit = -1;
    bottomExit = -1;
    
    xPos = xP;
    yPos = yP;
    
    complete = false;
}
room::room(vertex s, vertex e, int r, int g, int b, int xP, int yP) : me(s, e, r, g, b){
    leftExit = -1;
    rightExit = -1;
    topExit = -1;
    bottomExit = -1;
    
    xPos = xP;
    yPos = yP;
    
    complete = false;
}

room::~room(){
    /*delete leftDoor;
    delete rightDoor;
    delete bottomDoor;
    delete topDoor;*/
    //Need to comeback to this, for now I'm letting the system handle the deletion, but I shouldn't trust it
}

void room::drawRoom(){
    me.drawEntity();
    
    if(complete){ //display exits
        glColor3ub(255, 0, 255);
        if(leftExit >= 0){
            leftDoor->drawEntity();
        }
        if(rightExit >= 0){
            rightDoor->drawEntity();
        }
        if(topExit >= 0){
            topDoor->drawEntity();
        }
        if(bottomExit >= 0){
            bottomDoor->drawEntity();
        }
    }
}

void room::instantiateTopDoor(){
    topDoor = new entity((me.getEnd().getX() + me.getStart().getX())/ 2 - (xPortalSize / 2), (me.getEnd().getY() - yPortalOffset - yPortalSize), xPortalSize, yPortalSize, 255, 0, 255);
}
void room::instantiateBottomDoor(){
    bottomDoor = new entity((me.getEnd().getX() + me.getStart().getX())/ 2 - (xPortalSize / 2), (me.getStart().getY() + yPortalOffset), xPortalSize, yPortalSize, 255, 0, 255);//
}
void room::instantiateLeftDoor(){
    leftDoor = new entity(me.getStart().getX() + xPortalOffset, (me.getStart().getY() + me.getEnd().getY()) / 2 - (yPortalSize / 2), xPortalSize, yPortalSize, 255, 0, 255);
    
}
void room::instantiateRightDoor(){
    rightDoor = new entity(me.getEnd().getX() - xPortalOffset - xPortalSize, (me.getStart().getY() + me.getEnd().getY()) / 2 - (yPortalSize / 2), xPortalSize, yPortalSize, 255, 0, 255);
}
