//
//  enemy.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 11/1/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "enemy.hpp"

enemy::enemy(int x, int y, int h, int w, defaultColors c,  int hp) : me(x, y, h, w, c){
    speed = 0;

    health = hp;
    startingHealth = hp;
    damageTaken = 0;
    
    size = h;
}
enemy::enemy(vertex s, vertex e, defaultColors c,  int hp) : me(s, e, c){
    speed = 0;

    health = hp;
    startingHealth = hp;
    damageTaken = 0;
    
    size = e.getX();
}
enemy::enemy(int x, int y, int h, int w, int r, int g, int b, int hp) : me(x, y, h, w, r, g, b){
    speed = 0;

    health = hp;
    startingHealth = hp;
    damageTaken = 0;
    
    size = h;
}
enemy::enemy(vertex s, vertex e, int r, int g, int b,  int hp) : me(s, e, r, g, b){
    speed = 0;

    health = hp;
    startingHealth = hp;
    damageTaken = 0;
    
    size = e.getX();
}

void enemy::draw(){
    me.drawEntity();
    //draw dmg taken
    float healthMissing = 1 - float(damageTaken) / float(startingHealth);
    
    if(healthMissing != 1){
        int xSize = me.getEnd().getX() - me.getStart().getX(), ySize = me.getEnd().getY() - me.getStart().getY();
        glColor3ub(255, 255, 255);
        glBegin(GL_QUADS);
        glVertex2f(me.getStart().getX() + (xSize / 2 * healthMissing), me.getStart().getY() + (ySize / 2 * healthMissing));
        glVertex2f(me.getStart().getX() + (xSize / 2 * healthMissing), me.getEnd().getY() - (ySize / 2 * healthMissing));
        glVertex2f(me.getEnd().getX() - (xSize / 2 * healthMissing), me.getEnd().getY() - (ySize / 2 * healthMissing));
        glVertex2f(me.getEnd().getX() - (xSize / 2 * healthMissing), me.getStart().getY() + (ySize / 2 * healthMissing));
        glEnd();
    }
}

void enemy::move(direction d){
    switch (d) {
        case Up:
            me.up(speed);
            break;
        case Left:
            me.left(speed);
            break;
        case Right:
            me.right(speed);
            break;
        case Down:
            me.down(speed);
            break;
        default:
            break;
    }
}

void enemy::resetVerts(int x, int y, int w, int h){
    me.resetVerts(x, y, w, h);
}
void enemy::resetVerts(vertex s, vertex e){
    me.resetVerts(s, e);
}

bool enemy::entityCollison( entity one, entity two){
    if(one.getStart().getX() < two.getEnd().getX() &&
       one.getEnd().getX() > two.getStart().getX() &&
       one.getStart().getY() < two.getEnd().getY() &&
       one.getEnd().getY() > two.getStart().getY())
        return true;
    return false;
}
