/*
 
    Senior Indepdent Study Main.cpp
    Developer: Tommy Bacher
    Dependencies: OpenGL, GLUT

    Created August 29, 2017
    Modified September 13, 2017
 
 */

/*#include "play.hpp"

int mainMenuSelect;

void init(){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-(WINDOW_WIDTH / 2), WINDOW_WIDTH / 2, -(WINDOW_HEIGHT / 2), WINDOW_HEIGHT / 2);
    glMatrixMode(GL_MODELVIEW);
    glClearColor (0.0, 0.0, 0.0, 1.0);
    glColor3f(1.0,0.0,0.0);
    glEnable(GL_DEPTH_TEST);
    
    mainMenuSelect = 0;
    
}

void mainMenu(){
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    
    if (mainMenuSelect == 0)
        glColor3ub(255, 255, 255);
    else
        glColor3ub(255, 0, 0);
    glRasterPos2i(0,0);
    std::string sToPrint = "Play New Game";
    for (int i = 0; i < sToPrint.length(); i++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, sToPrint[i]);
        //rx+=glutBitmapWidth(GLUT_BITMAP_9_BY_15,k);
    }
    
    if (mainMenuSelect == 1)
        glColor3ub(255, 255, 255);
    else
        glColor3ub(255, 0, 0);
    sToPrint = "Settings";
    glRasterPos2i(0,-20);
    for (int i = 0; i < sToPrint.length(); i++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, sToPrint[i]);
        //rx+=glutBitmapWidth(GLUT_BITMAP_9_BY_15,k);
    }
    
    if (mainMenuSelect == 2)
        glColor3ub(255, 255, 255);
    else
        glColor3ub(255, 0, 0);
    glRasterPos2i(0,-40);
    sToPrint = "Exit";
    for (int i = 0; i < sToPrint.length(); i++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, sToPrint[i]);
        //rx+=glutBitmapWidth(GLUT_BITMAP_9_BY_15,k);
    }
    
    
    glutSwapBuffers();

}

void mainMenuKeyboard(unsigned char key, int x, int y){
    switch (key) {
        case 's':
            mainMenuSelect = (mainMenuSelect + 1) % 3;
            glutPostRedisplay();
            break;
        case 'w':
            mainMenuSelect -= 1;
            if (mainMenuSelect < 0) mainMenuSelect = 2;
            glutPostRedisplay();
            break;
        case 13: //13 is the enter key
            switch (mainMenuSelect) {
                case 0:
                    initPlay();
                    glutDisplayFunc(play);
                    glutKeyboardFunc(playKeyboard);
                    glutKeyboardUpFunc(playKeyboardUp);
                    glutIdleFunc(playIdle);
                    glutPostRedisplay();
                    break;
                case 1:
                    //Settings
                    break;
                case 2:
                    exit(0);
                    break;
                default:
                    break;
            }
        default:
            break;
    }
}

//Keep window from reshaping
void myReshape(int w, int h) {
    glutReshapeWindow(WINDOW_WIDTH, WINDOW_HEIGHT);
}

int main(int argc, char** argv){
    
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GL_DOUBLE);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("simple");
    glutDisplayFunc(mainMenu);
    glutKeyboardFunc(mainMenuKeyboard);
    glutReshapeFunc(myReshape);
    
    init();
    
    glutMainLoop();

}*/
