//
//  vertex.hpp
//  seniorIS
//
//  Created by John Bacher on 9/26/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef vertex_hpp
#define vertex_hpp


#include "color.hpp"

class vertex {
    int x, y; // Position Components
    color myColor;
    
public:
    vertex(int x, int y);
    vertex(int x, int y, defaultColors c);
    vertex(int x, int y, int r, int g, int b);
    
    void setX(int x){ (*this).x = x; }
    void setY(int y){ (*this).y = y; }
    
    int getX(){return x; }
    int getY(){return y; }
    
    void up(int delta){y += delta;}
    void down(int delta){y -= delta;}
    void left(int delta){x -= delta;}
    void right(int delta){x += delta;}
    
    //Creates vertex at this vertex
    //if changeColor == true then this vertex's color will be applied
    void createVertex(bool changeColor);
};

#endif /* vertex_hpp */
