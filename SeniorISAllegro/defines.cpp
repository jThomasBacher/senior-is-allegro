#include "defines.h"

//int winHeight = 600;//600;
//int winWidth = 800;//800;

int viewHeight = 1800;//winHeight * 2;
int viewWidth = 2400;//winWidth * 2;

int xPortalOffset = 20;
int yPortalOffset = 20;

int xPortalSize = 160;
int yPortalSize = 160;

float FPS = 60; //Could change, right now we're aiming for 60 fps

ALLEGRO_TIMER *gameTimer = al_create_timer(GAME_TIME_INTERVAL);



void reduceFraction (int &numerator, int &denominator)
{
    int gcDenom = gcd(numerator, denominator);
    numerator /= gcDenom;
    denominator /= gcDenom;
}

int gcd(int u, int v)
{
    return (v != 0) ? gcd(v, u % v) : u;
}

#ifdef __APPLE__
int winHeight = 1200;
int winWidth = 1600;
float speedFactor = 1;
int machineType =0;
#elif  _WIN32 || _WIN64 //Need to fix this, as it assumes windows
int winHeight = 600;
int winWidth = 800;
float speedFactor = 3;
int machineType = 1;
#endif
