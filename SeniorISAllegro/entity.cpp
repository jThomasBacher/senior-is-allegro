//
//  entity.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 10/17/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "entity.hpp"

entity::entity(int x, int y, int w, int h, defaultColors c) : backColor(c), start(x, y), end(x + w, y + h){
}
entity::entity(vertex s, vertex e, defaultColors c) : backColor(c), start(s), end(e){
    
}
entity::entity(int x, int y, int w, int h, int r, int g, int b) : backColor(r, g, b), start(x, y), end(x + w, y + h){
}
entity::entity(vertex s, vertex e, int r, int g, int b) : backColor(r, g, b), start(s), end(e){
}

void entity::drawEntity(){
    glBegin(GL_QUADS);
    backColor.setGLColor();
    start.createVertex(false);
    
    vertex newX(end.getX(), start.getY());
    newX.createVertex(false);
    
    end.createVertex(false);
    
    vertex newY(start.getX(), end.getY());
    newY.createVertex(false);
    glEnd();
}

void entity::resetVerts(int x, int y, int w, int h){
    start.setX(x);
    start.setY(y);
    end.setX(x + w);
    end.setY(y + h);
}
void entity::resetVerts(vertex s, vertex e){
    start.setX(s.getX());
    start.setY(s.getX());
    end.setX(e.getX());
    end.setY(e.getY());
}
