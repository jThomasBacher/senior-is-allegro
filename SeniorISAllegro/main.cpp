

#include "gameMaster.hpp"
#include "defines.h"
#include <iostream>





int main(int argc, char **argv) {
    if(!al_init()){
        return false;
    }
    al_install_keyboard();
	if(!al_init_primitives_addon())
        fprintf(stderr, "Could not load primitives addon\n");
    if(!al_init_font_addon()) // initialize the font addon
        fprintf(stderr, "Could not load font addon\n");
    if(!al_init_ttf_addon())// initialize the ttf (True Type Font) addon
        fprintf(stderr, "Could not load ttf addon\n");

    std::cout << winWidth << "\t" << winHeight << std::endl;

	al_set_new_display_flags(ALLEGRO_OPENGL | ALLEGRO_WINDOWED);

    ALLEGRO_DISPLAY* display = al_create_display(winWidth, winHeight);
    
    if(display == NULL)
        fprintf(stderr, "Display did not init\n");
	
    //init(); // Init OpenGL vars

	//ALLEGRO_EVENT_QUEUE* displayEQ = al_create_event_queue();


	//ALLEGRO_EVENT_QUEUE* mainQueue = al_create_event_queue();

	//al_register_event_source(mainQueue, al_get_timer_event_source(timer));
    



    gameMaster testOne(display);
    
	
    
    //while (true) {// Start Main Game Loops
		

    (testOne.play()); //Play handles user input, while updating the game state

    gameMaster testTwo(display);
    
    testTwo.play();
    
    gameMaster testThree(display);
    
    testThree.play();
    
    
    al_destroy_display(display);
    
    return 0;
}
