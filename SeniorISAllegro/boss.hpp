//
//  boss.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 11/29/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef boss_hpp
#define boss_hpp

#include "enemy.hpp"

class boss : public enemy {
    //Current Implementation of boss has four weapons
    //One for firing in each direction ->
    weapon northGun, southGun, westGun, eastGun;
    
    vertex targetLoc;
    unsigned long startTime;
    int timeToMove;
    
public:
    boss(int x, int y, int h, int w, defaultColors c, int hp, std::string n, int sS, int ran, int s, int sD, int dmg);
    boss(vertex start, vertex e, defaultColors c,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg);
    boss(int x, int y, int h, int w, int r, int g, int b,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg);
    boss(vertex start, vertex e, int r, int g, int b,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg);
    
    void draw();
    
    //bool entityCollison(entity one, entity two);
    
    void behavior(player *target);
};

#endif /* boss_hpp */
