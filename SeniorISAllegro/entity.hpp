//
//  entity.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 10/17/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef entity_hpp
#define entity_hpp

#include "vertex.hpp"

class entity {
    vertex start, end;
    color backColor;

public:
    entity(int x, int y, int w, int h, defaultColors c);
    entity(vertex s, vertex e, defaultColors c);
    entity(int x, int y, int w, int h, int r, int g, int b);
    entity(vertex s, vertex e, int r, int g, int b);

    void drawEntity(); //Draws this entity with color backColor;

    void resetVerts(int x, int y, int w, int h);
    void resetVerts(vertex s, vertex e);

    vertex getStart(){return start;}
    vertex getEnd(){return end;}

    int getCenterX(){return (start.getX() + end.getX()) / 2;}
    int getCenterY(){return (start.getY() + end.getY()) / 2;}
    vertex getCenter(){return vertex(getCenterX(), getCenterY());}

	int getWidth() { return end.getX() - start.getX(); }
	int getHeight() { return end.getY() - start.getY(); }

    void up(int delta){start.up(delta); end.up(delta);}
    void down(int delta){start.down(delta); end.down(delta);}
    void left(int delta){start.left(delta); end.left(delta);}
    void right(int delta){start.right(delta); end.right(delta);}

    void setColor(int r, int g, int b){backColor.setColor(r, g, b);}
    void setColor(defaultColors c){backColor.setColor(c);}
};
#endif /* entity_hpp */
