//
//  weapon.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 11/2/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "weapon.hpp"

weapon::weapon(std::string n, int sS, int ran, int s, int sD, int dmg){
    name = n;
    shotSpeed = sS;
    range = ran;
    size = s;
    shotDelay = sD;
    damage = dmg;
    
    startTime = al_get_timer_count(gameTimer);
}

void weapon::fire(entity user, direction heading, int r, int g, int b){
    bool left = false, up = false;
    if (al_get_timer_count(gameTimer) > startTime + shotDelay) {
        int rise, run;
        switch (heading) {
            case Left:
                left = true;
                run = -1;
                rise = 0;
                break;
            case Right:
                left = false;
                run = 1;
                rise = 0;
                break;
            case Up:
                up = true;
                rise = 1;
                run = 0;
                break;
            case Down:
                up = false;
                rise = -1;
                run = 0;
                break;
            default:
                rise = 0;
                run = 0;
                break;
        }
        shots.push_back(projectile(user.getCenterX() - size / 2, user.getCenterY() - size / 2, size, size, r, g, b, rise, run, shotSpeed, left, up));
        startTime = al_get_timer_count(gameTimer);
    }
}

void weapon::fire(entity user, vertex target, int r, int g, int b){
    if (al_get_timer_count(gameTimer) > startTime + shotDelay) {
        bool left = target.getX() < user.getCenterX(), up = target.getY() >= user.getCenterY();
        
        int rise, run;
        rise = target.getY() - user.getCenterY();
        run = target.getX() - user.getCenterX();
        if(run == 0)
            if(signbit(rise) == 0)
                rise = 1;
            else
                rise = -1;
            else if (rise == 0)
                if(signbit(run) == 0)
                    run = 1;
                else
                    run = -1;
                else
                    reduceFraction(rise, run);
        
    
        
        shots.push_back(projectile(user.getCenterX() - size / 2, user.getCenterY() - size / 2, size, size, r, g, b, rise, run, shotSpeed, left, up));
        startTime = al_get_timer_count(gameTimer);
    }
}

void weapon::update(){
    std::vector<int> toRemove;
    for (int i = 0; i < shots.size(); i++) {
        shots[i].update();
        if (shots[i].getDist() >= range) {
            toRemove.push_back(i);
        }
    }
    for (int i = 0; i < toRemove.size(); i++) {
        deleteProjectile(toRemove[i]);
    }
}

void weapon::deleteProjectile(int i){
    shots.erase(shots.begin() + i);
}
