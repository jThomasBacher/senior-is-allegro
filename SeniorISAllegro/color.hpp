//
//  color.hpp
//  seniorIS
//
//  Created by John Bacher on 9/26/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef color_hpp
#define color_hpp

#include "defines.h"


enum defaultColors { Red, Green, Blue, Black, Yellow, White, EnemyColor, DarkGreen};

class color {
    int red, green, blue;
    
public:
    color(defaultColors c);
    color(int r, int g, int b);
    
    void setColor(defaultColors c);
    void setColor(int r, int g, int b);
    
    int redComp(){return red;}
    int greenComp(){return green;}
    int blueComp(){return blue;}
    
    void setGLColor(){ glColor3ub(red, green, blue); }
};

#endif /* color_hpp */
