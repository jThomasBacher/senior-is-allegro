//
//  enemy.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 11/1/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef enemy_hpp
#define enemy_hpp

#include "player.hpp"
#include "weapon.hpp"

//enum movementBehavior {stationary, chase, run};

class enemy {
    entity me;
    
    int speed;
    int health, startingHealth, damageTaken, size;
    
    bool addedToTotalDead = false;
    
public:
    enemy(int x, int y, int h, int w, defaultColors c, int hp);
    enemy(vertex s, vertex e, defaultColors c, int hp);
    enemy(int x, int y, int h, int w, int r, int g, int b, int hp);
    enemy(vertex s, vertex e, int r, int g, int b, int hp);
    
    virtual void draw();
    
    void move(direction d);
    
    void resetVerts(int x, int y, int w, int h);
    void resetVerts(vertex s, vertex e);
    
    entity getMe(){return me;}
    int getSpeed(){return speed;}
    
    void adjustSpeed(int delta){speed += (delta * speedFactor);}
    void adjustHealth(int delta){health += delta; damageTaken += (-delta); }
    
    int getCenterX(){return me.getCenterX();}
    int getCenterY(){return me.getCenterY();}
    
    //Depending on the various behaviors of this enemy, the adjust
    virtual void behavior(player *target) = 0;
    
    void setCounted(){addedToTotalDead = true;}
    bool isAddedToTotalDead(){return addedToTotalDead;}
    
    bool isAlive(){return health > 0;}
    
    bool entityCollison( entity one, entity two);
    
    int getSize(){return size;}
    int getHealth(){return health;}
    virtual weapon* getWeapon(){return nullptr;}
};

#endif /* enemy_hpp */
