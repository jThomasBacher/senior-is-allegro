//
//  data.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 1/28/18.
//  Copyright © 2018 Tommy Bacher. All rights reserved.
//

#ifndef data_hpp
#define data_hpp

#include "defines.h"
#include <string>
#include <iostream>
#include <vector>
#include <fstream>

class data {
    std::string delim;
    std::vector<std::string> stream;
    
    int currentIndex;
public:
    data();
    data(std::string d);
    
    void pushBack(std::string s);
    bool saveToFile();
    
    std::string getDelim(){return delim;}
    void setDelim(std::string s){delim = s;}
};

#endif /* data_hpp */
