//
//  gameMaster.cpp
//  seniorIS
//
//  Created by John Bacher on 9/26/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "gameMaster.hpp"

gameMaster::gameMaster(ALLEGRO_DISPLAY *d) : playerOne(viewWidth / 2 - 40, viewHeight / 2 - 40, 80, 80, Green){
    errorLog.setDelim("Error Log ");
    
    
    
    //playerOne.adjustHealth(10);
    window = d;
	ALLEGRO_TIMER *fpsTimer = al_create_timer(1.0 / FPS);
    gameFont = al_load_ttf_font("LinLibertine_DRah.ttf",55,0);
    
    if (!gameFont){
        fprintf(stderr, "Could not load 'LinLibertine_DRah.ttf'.\n\nIn Directory %s\n\n", al_get_current_directory());
        errorLog.pushBack("There was an Error Loading the font: 'LinLibertine_DRah.ttf'. Text will not appear in the game as a result. \n\tTo fix this error, place 'LinLibertine_DRah.ttf' at the following path in your computer: \n\t");
        errorLog.pushBack(al_get_current_directory());
        errorLog.pushBack("\n");
    }
    al_get_current_directory();

    fprintf(stderr, "One\n");
    eventQueue = al_create_event_queue();
    if (!eventQueue)
        fprintf(stderr, "Could not initialize event queue\n");
    fprintf(stderr, "Two\n");

    al_register_event_source(eventQueue, al_get_display_event_source(window));
    fprintf(stderr, "Three\n");

    al_register_event_source(eventQueue, al_get_keyboard_event_source());
    fprintf(stderr, "Four\n");

	al_register_event_source(eventQueue, al_get_timer_event_source(fpsTimer));
    fprintf(stderr, "Five\n");


    numberOfKeys = 10;
    key = new bool[numberOfKeys];
    for (int i = 0; i < numberOfKeys; i++) {
        key[i] = false;
    }
    
    
    
    //RANDOMIZE EVERYTHING - Also store the seed so as to theoretically be able to play this version again
    time_t seed = time(NULL);
    srand(seed);
    
    testData.pushBack("<seed>\n");
    testData.pushBack(std::to_string(seed));
    testData.pushBack("\n</seed>\n");
    
    

	roomFinished = false;

    //May need to pass these off to a seperate, create new game function, and let the game master be in charge of jumping between the main menu, settings, exit, and gameplay, that way I don't have to pass window around a bunch and have a bunch of seperate classes referencing the same vars and serving basically the same purpose
    room startRoom(20, 20, viewWidth - 40, viewHeight - 40, White, 0, 0);
    floor.push_back(startRoom);
    nextEmptyRoom = 1;
    currRoom = 0;
    
    enemyBaseHealth = 4;
	
    
    
    //enemies.push_back(std::make_shared<chasingEnemy>(viewWidth / 8 - 10, viewWidth / 8 - 10, 200, 200, 255, 0, 50, enemyBaseHealth));
    //enemies.push_back(std::make_shared<gunEnemy>(viewWidth / 8 - 10, viewWidth / 8 - 10, 60, 60, 255, 0, 50, enemyBaseHealth, "prime", 4, viewWidth / 4, 16, 0.65 / GAME_TIME_INTERVAL, 1, &playerOne));
    //enemies.push_back(std::make_shared<boss>(viewWidth / 8 - 10, viewWidth / 8 - 10, 30, 30, 255, 0, 50, 50, "Gun", 5, viewWidth, 30, 200, 1));
    //enemies[0]->adjustSpeed(1);
    //enemies.push_back(std::make_shared<enemy>(viewWidth / 8 - 10, viewWidth / 8 - 10, 30, 30, 255, 0, 50, enemyBaseHealth));
    //enemies[1]->adjustSpeed(1);
    
    //Can fire ever 0.75 seconds,

    weapons.push_back(weapon("Base Weapon", 5, viewWidth, 20, 0.5 / GAME_TIME_INTERVAL, 1));
    weaponEquipped = 0;

    //weapon w("Base Weapon", 4, viewWidth / 2, 250, 20, 1);
    
    //This is the basic idea for enemyPointTemplate
    //Of note is that I'm currently not multiplying by roomsWithoutDamage because it is currently 0
    //and it's only worth multiplying by it when it is >=2, the multiplication by 10 just gives me more
    //"room" to work with, it has no actual effect
    //enemyPointTemplate = (weapons[0].getDmg() + playerOne.getSpeed() + playerOne.getHealth()) * 10;
    
    enemyPointTemplate = 80; //Starting Point
    
    roomsWithoutDamage = 0;

    enemiesDefeated = 0;
    enemiesToSpawnBoss = 50;
    
    pointsDefeated = 0;
    pointsToSpawnBoss = 1600;
	//printf("enemiesLength: %d", enemies.size());
    
    /*FOR TESTING*/
    //enemyPointTemplate = 30;
    //generateEnemies();
    
    roomFinished = true;

    //updatePlayerData();
    
   
    
	al_start_timer(fpsTimer); //Begin timers
    al_start_timer(gameTimer);
    
    fprintf(stderr, "Finishing Init of Game Master\n");
}

gameMaster::~gameMaster(){
    delete [] key;
    floor.clear();
    enemies.clear();
}

void gameMaster::display(){
    
    
    floor[currRoom].drawRoom();
    
   // fprintf(stderr, "room Drawn\n");
    
    playerOne.drawHealth();
    
   // fprintf(stderr, "health Drawn\n");
    
    for (int i = 0; i < enemies.size(); i++) {
        if(enemies[i]->isAlive())
            enemies[i]->draw();
    }
    
   // fprintf(stderr, "enemies Drawn\n");
    
    for (int i = 0; i < weapons[weaponEquipped].getShots().size(); i++) {
        weapons[weaponEquipped].getShots()[i].draw();
    }
    
  //  fprintf(stderr, "weapon Drawn\n");
	if (powerUpSpawned) {
		generalUp->drawPowerUp();
	}
    
    playerOne.drawPlayer();
   // fprintf(stderr, "player Drawn\n");
    
    
    if(powerUpSpawned){
        std::string powerUpType;
        switch (generalUp->getType()) {
            case Damage:
                powerUpType = "Damage";
                break;
            case Range:
                powerUpType = "Range";
                break;
            case ShotSpeed:
                powerUpType = "Shot Speed";
                break;
            case FireRate:
                powerUpType = "Fire Rate";
                break;
            case Health:
                powerUpType = "Health";
                break;
            case ShotSize:
                powerUpType = "Shot Size";
                break;
            case MovementSpeed:
                powerUpType = "Movement Speed";
                break;
            default:
                break;
        }

        al_draw_textf(gameFont, al_map_rgb(255, 255, 255), 800, winHeight - 90, 0, "Power Up is: %s", powerUpType.c_str() );
    }
    
    if(roomFinished && gameFont){
        if(pointsToSpawnBoss > pointsDefeated){
            al_draw_textf(gameFont, al_map_rgb(255, 255, 255), 50, winHeight - 90, 0, "You're %.2f%% of the way to the boss fight", 100 * float(pointsDefeated)/float(pointsToSpawnBoss));
        }
        else if (!bossSpawned){
            al_draw_text(gameFont, al_map_rgb(255, 255, 255), 50, winHeight - 90, 0, "Boss will be encountered in the next room!");
        }
        else{
            al_draw_text(gameFont, al_map_rgb(255, 255, 255), 50, winHeight - 90, 0, "CONGRATULATIONS! You Won! Press Space Bar to quit");
        }
        
    }
   // fprintf(stderr, "text Drawn\n");
}

bool gameMaster::play(){
    fprintf(stderr, "Entering Play\n");
    ALLEGRO_EVENT event;
    bool gameGoing = true;
    
    //Loop to process all events in the queue, this should probably be an if, but I want to try this first
    fprintf(stderr, "Entering Main Game Loop\n");
	while (gameGoing) {
        //printf("\nTimer1: %d\tTimer2: %d", al_get_timer_count(t), al_get_timer_count(t2));
        if(machineType == 0){
            al_set_target_backbuffer(window);
            al_clear_to_color(al_map_rgb(255, 255, 255));
            
            resetOpenGLMatrices();
           // fprintf(stderr, "Before Display\n");
            display(); //Display pushes the updated game state to the allegro/opengl view
           // fprintf(stderr, "After Display\n");
            al_flip_display(); //Swap buffers to actually show new game state
        }
		if (al_get_next_event(eventQueue, &event)) {
			//event is not Null; i.e. gamestate has changed
			if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
				exit(EXIT_SUCCESS);
			}
			else if (machineType == 1 && event.type == ALLEGRO_EVENT_TIMER) {
				al_set_target_backbuffer(window);
				al_clear_to_color(al_map_rgb(255, 255, 255));

				resetOpenGLMatrices();

				display(); //Display pushes the updated game state to the allegro/opengl view
				al_flip_display(); //Swap buffers to actually show new game state
			}
			else if (event.type == ALLEGRO_EVENT_KEY_DOWN) {
				switch (event.keyboard.keycode) {
				case ALLEGRO_KEY_W:
					key[keyW] = true;
					break;
				case ALLEGRO_KEY_A:
					key[keyA] = true;
					break;
				case ALLEGRO_KEY_S:
					key[keyS] = true;
					break;
				case ALLEGRO_KEY_D:
					key[keyD] = true;
					break;
				case ALLEGRO_KEY_SPACE:
					key[keySpace] = true;
					break;
				case ALLEGRO_KEY_ESCAPE:
					key[keyEsc] = true;
					break;
				case ALLEGRO_KEY_LEFT:
					key[keyLeft] = true;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[keyRight] = true;
					break;
				case ALLEGRO_KEY_UP:
					key[keyUp] = true;
					break;
				case ALLEGRO_KEY_DOWN:
					key[keyDown] = true;
					break;
				case ALLEGRO_KEY_Z:
					//printf("Current Room: %d\n", currRoom);
					//playerOne.adjustSpeed(1);
					break;
				default:
					break;
				}
			}
			else if (event.type == ALLEGRO_EVENT_KEY_UP) {
				switch (event.keyboard.keycode) {
				case ALLEGRO_KEY_W:
					key[keyW] = false;
					break;
				case ALLEGRO_KEY_A:
					key[keyA] = false;
					break;
				case ALLEGRO_KEY_S:
					key[keyS] = false;
					break;
				case ALLEGRO_KEY_D:
					key[keyD] = false;
					break;
				case ALLEGRO_KEY_SPACE:
					key[keySpace] = false;
					break;
				case ALLEGRO_KEY_ESCAPE:
					key[keyEsc] = false;
					break;
				case ALLEGRO_KEY_LEFT:
					key[keyLeft] = false;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[keyRight] = false;
					break;
				case ALLEGRO_KEY_UP:
					key[keyUp] = false;
					break;
				case ALLEGRO_KEY_DOWN:
					key[keyDown] = false;
					break;
				default:
					break;
				}
			}
		}
			// Note to self: I am trying to treat this loop as the actual game loop, as a result I'm going to change it to a while(true) for now (potentially add match over var)
			//						and if this fixes my windows problems I'll start working on a better solution for cross platform.

			if (key[keyEsc] && key[keySpace]) {
                testData.saveToFile();
                errorLog.saveToFile();
                return true;
			}
			if (key[keyW]) {
				playerOne.move(Up);
				if (!playerContainedWithinRoom(playerOne, floor[currRoom])) {
					playerOne.move(Down);
				}
			}
			if (key[keyA]) {
				playerOne.move(Left);
				if (!playerContainedWithinRoom(playerOne, floor[currRoom])) {
					playerOne.move(Right);
				}
			}
			if (key[keyS]) {
				playerOne.move(Down);
				if (!playerContainedWithinRoom(playerOne, floor[currRoom])) {
					playerOne.move(Up);
				}
			}
			if (key[keyD]) {
				playerOne.move(Right);
				if (!playerContainedWithinRoom(playerOne, floor[currRoom])) {
					playerOne.move(Left);
				}
			}
			if (key[keyLeft]) {
				weapons[weaponEquipped].fire(playerOne.getMe(), Left, 255, 255, 255);
			}
			if (key[keyRight]) {
				weapons[weaponEquipped].fire(playerOne.getMe(), Right, 255, 255, 255);
			}
			if (key[keyUp]) {
				weapons[weaponEquipped].fire(playerOne.getMe(), Up, 255, 255, 255);
			}
			if (key[keyDown]) {
				weapons[weaponEquipped].fire(playerOne.getMe(), Down, 255, 255, 255);
			}

			if (key[keySpace]) {
				//Checks to make sure that the room is finished, then generates a new room
				if ( roomFinished) {
					if(!bossSpawned)
                        generateNewRoom();
                    else{
                        testData.saveToFile();
                        errorLog.saveToFile();
                        return true;
                    }
				}

			}

			weapons[weaponEquipped].update();
			std::vector<int> toRemove;
			bool pushed = false;
			//check for collison between various entities, doing it here so enemies can be removed if they die
			for (int i = 0; i < weapons[weaponEquipped].getShots().size(); i++) {
				for (int j = 0; j < enemies.size(); j++) {

					if (enemies[j]->isAlive() && entityCollison(enemies[j]->getMe(), weapons[weaponEquipped].getShots()[i].getMe())) {
						enemies[j]->adjustHealth(-weapons[weaponEquipped].getDmg());
						if (!pushed) {
							toRemove.push_back(i);
							pushed = true;
						}
					}
				}
				pushed = false;
			}
			//printf("Shots on screen: %d\n", weapons[weaponEquipped].getShots().size());
			for (int i = 0; i < toRemove.size(); i++) {
				weapons[weaponEquipped].deleteProjectile(toRemove[i]);
			}

			updateEnemies();
			updatePowerUps();
            if(playerOne.getHealth() <= 0)
                gameGoing = false;
        
    }

	
    testData.saveToFile();
    errorLog.saveToFile();
    return true;
}

bool gameMaster::playerContainedWithinRoom(player p, room r){
    //printf("Value 1: %d\nValue 2: %d\nValue 3: %d + %d\nValue 4: %d + %d\n\n", p.getStartBound().getX(), r.getStart().getX(), p.getStartBound().getX() , p.getEndBound().getX(), r.getStart().getX() , r.getEnd().getX());
    return p.getMe().getStart().getX() > r.getMe().getStart().getX() &&
    p.getMe().getEnd().getX() < r.getMe().getEnd().getX() &&
    p.getMe().getStart().getY() > r.getMe().getStart().getY() &&
    p.getMe().getEnd().getY() < r.getMe().getEnd().getY();
}

bool gameMaster::playerContainedWithinEntity(player p, entity e){
    //printf("Value 1: %d\nValue 2: %d\nValue 3: %d + %d\nValue 4: %d + %d\n\n", p.getStartBound().getX(), r.getStart().getX(), p.getStartBound().getX() , p.getEndBound().getX(), r.getStart().getX() , r.getEnd().getX());
    return p.getMe().getStart().getX() > e.getStart().getX() &&
    p.getMe().getEnd().getX() < e.getEnd().getX() &&
    p.getMe().getStart().getY() > e.getStart().getY() &&
    p.getMe().getEnd().getY() < e.getEnd().getY();
}

int gameMaster::checkDoors(player p){
  //  printf("\n\n %d \n\n", floor[currRoom].getTopExit());
    //printf("1. My Left: %d\tRight: %d\tTop: %d\tBottom: %d\n", floor[currRoom].getLeftExit(), floor[currRoom].getRightExit(), floor[currRoom].getTopExit(), floor[currRoom].getBottomExit());
    if(floor[currRoom].getLeftExit() >= 0 && playerContainedWithinEntity(p, floor[currRoom].getLeftDoor()))
       return 0;
    else if(floor[currRoom].getRightExit() >= 0 && playerContainedWithinEntity(p, floor[currRoom].getRightDoor()))
        return 1;
    else if(floor[currRoom].getTopExit() >= 0 && playerContainedWithinEntity(p, floor[currRoom].getTopDoor()))
        return 2;
    else if(floor[currRoom].getBottomExit() >= 0 && playerContainedWithinEntity(p, floor[currRoom].getBottomDoor()))
        return 3;
    else
        return -1;
}

//Old implementation
/*
void gameMaster::finishRoom(){
    if(bossSpawned)
        exit(0);
    if (floor[currRoom].getLeftExit() >= 0) {
        floor[currRoom].instantiateLeftDoor();
    }
    if (floor[currRoom].getRightExit() >= 0) {
        floor[currRoom].instantiateRightDoor();
    }
    if (floor[currRoom].getTopExit() >= 0) {
        floor[currRoom].instantiateTopDoor();
    }
    if (floor[currRoom].getBottomExit() >= 0) {
        floor[currRoom].instantiateBottomDoor();
    }

    int possibleExits = 0;

    //Issue: what if there is no room at index? need to revise these checks
    /*
        =-1 and noRoomAtIndex OR =-1 and P
        P And Q OR P AND Z
        P AND (Q OR Z) -> this will short circuit i there is noRoomAtIndex which is what we want
     =-1 and (noRoomAtIndex || P)
     
    if (floor[currRoom].getLeftExit() == -1 && (roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos()) == -1 || floor[roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos())].getRightExit() != -2 )) {
        possibleExits += 1;
    }
    if (floor[currRoom].getRightExit() == -1 && ( roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos()) == -1 || floor[roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos())].getLeftExit() != -2)) {
        possibleExits += 1;
    }
    if (floor[currRoom].getTopExit() == -1 &&(roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() + 1) == -1 || floor[roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() + 1)].getBottomExit() != -2) ){
        possibleExits += 1;
    }
    if (floor[currRoom].getBottomExit() == -1 &&(roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() - 1) == -1 || floor[roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() - 1)].getTopExit() != -2) ){
        possibleExits += 1;
    }
    if(possibleExits == 0)
        return;

    printf("\n%d\n", possibleExits);
    printf("Left Room's right: %d\tRight Room's left: %d\tTop Room's bottom: %d\tBot Room's top: %d\n", floor[roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos())].getRightExit(), floor[roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos())].getLeftExit(), floor[roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() + 1)].getBottomExit(), floor[roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() - 1)].getTopExit());

    int numExits = (rand() % possibleExits) + 1;

    if(currRoom == 0)
        numExits = 4;



    int nextExit = rand() % 4; //0 is left, 1 is right, so on

    /*
        The error with rooms is that I'm not setting connection to -2 when a room determines that it doesn't want to have a lower room  and you go to the room  that exist there.

     

    while (numExits > 0) {
       // printf("CurrentExit: %d\t", nextExit);
        while((nextExit == 0 && floor[currRoom].getLeftExit() != -1 &&( roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos()) == -1 || floor[roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos())].getRightExit() != -2))
              || (nextExit == 1 && floor[currRoom].getRightExit() != -1 &&(roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos()) == -1 || floor[roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos())].getLeftExit() != -2))
              || (nextExit == 2 && floor[currRoom].getTopExit() != -1 &&(roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() + 1) == -1 || floor[roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() + 1)].getBottomExit() != -2))
              || (nextExit == 3 && floor[currRoom].getBottomExit() != -1 &&(roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() - 1) == -1 || floor[roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() - 1)].getTopExit() != -2))){
            nextExit++;
            if(nextExit == 4) nextExit = 0;
        }


        if (nextExit == 0){
            printf("leftexit\n");
            room newRoom(40, 40, viewWidth - 80, viewHeight - 80, 0, 0, 0, floor[currRoom].getXPos() - 1, floor[currRoom].getYPos());
            newRoom.setRightExit(currRoom);
            newRoom.setLeftExit(roomAtIndex(floor[currRoom].getXPos() - 2, floor[currRoom].getYPos()));
            newRoom.setTopExit(roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos() + 1));
            newRoom.setBottomExit(roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos() - 1));

            floor[currRoom].instantiateLeftDoor();

            floor[currRoom].setLeftExit(nextEmptyRoom);
            nextEmptyRoom++;
            floor.push_back(newRoom);
        }
        else if (nextExit == 1){
            printf("rightexit\n");
            room newRoom(40, 40, viewWidth - 80, viewHeight - 80, 0, 0, 0, floor[currRoom].getXPos() + 1, floor[currRoom].getYPos());
            newRoom.setLeftExit(currRoom);
            newRoom.setRightExit(roomAtIndex(floor[currRoom].getXPos() + 2, floor[currRoom].getYPos()));
            newRoom.setTopExit(roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos() + 1));
            newRoom.setBottomExit(roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos() - 1));

            floor[currRoom].instantiateRightDoor();

            floor[currRoom].setRightExit(nextEmptyRoom);
            nextEmptyRoom++;
            floor.push_back(newRoom);
        }
        else if (nextExit == 2){
            printf("topexit\n");
            room newRoom(40, 40, viewWidth - 80, viewHeight - 80, 0, 0, 0, floor[currRoom].getXPos(), floor[currRoom].getYPos() + 1);
            newRoom.setBottomExit(currRoom);
            newRoom.setRightExit(roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos() + 1));
            newRoom.setTopExit(roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() + 2));
            newRoom.setLeftExit(roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos() + 1));

            //printf("NewRoomTop: %d\nNewRoomBot: %d\nNewRoomLeft: %d\nNewRoomRight: %d\n", newRoom->getTopExit(), newRoom->getBottomExit(), newRoom->getLeftExit(), newRoom->getRightExit());

            floor[currRoom].instantiateTopDoor();

            floor[currRoom].setTopExit(nextEmptyRoom);
            nextEmptyRoom++;
            floor.push_back(newRoom);
        }
        else if (nextExit == 3){
            printf("botexit\n");
            room newRoom(40, 40, viewWidth - 80, viewHeight - 80, 0, 0, 0, floor[currRoom].getXPos(), floor[currRoom].getYPos() - 1);
            newRoom.setTopExit(currRoom);
            newRoom.setRightExit(roomAtIndex(floor[currRoom].getXPos() + 1, floor[currRoom].getYPos() - 1));
            newRoom.setLeftExit(roomAtIndex(floor[currRoom].getXPos() - 1, floor[currRoom].getYPos() - 1));
            newRoom.setBottomExit(roomAtIndex(floor[currRoom].getXPos(), floor[currRoom].getYPos() - 2));

            floor[currRoom].instantiateBottomDoor();

            floor[currRoom].setBottomExit(nextEmptyRoom);
            nextEmptyRoom++;
            floor.push_back(newRoom);
        }



        //delete newRoom;

        numExits--;

        nextExit = rand() % 4;
    }

    //Set Empty Doors - HAVE NOT TESTED
    if (floor[currRoom].getLeftExit() == -1) {
        floor[currRoom].setLeftExit(-2);
    }
    if (floor[currRoom].getRightExit() == -1) {
        floor[currRoom].setRightExit(-2);
    }
    if (floor[currRoom].getTopExit() == -1) {
        floor[currRoom].setTopExit(-2);
    }
    if (floor[currRoom].getBottomExit() == -1) {
        floor[currRoom].setBottomExit(-2);
    }

    //printf("My Left: %d\tRight: %d\tTop: %d\tBottom: %d\n", floor[currRoom].getLeftExit(), floor[currRoom].getRightExit(), floor[currRoom].getTopExit(), floor[currRoom].getBottomExit());
    
    //Dynamic Adjustment Variable Update
    if(playerOne.getDamageTaken() == 0)
        roomsWithoutDamage++;
    else
        roomsWithoutDamage = 0;
    
    floor[currRoom].finish();
}
*/

//New Implementation
//This is called when:
//	All Enemies are dead
void gameMaster::finishRoom() {
    //printf("Enemy Point Temp: %d", enemyPointTemplate);
    updatePlayerData();
    pointsDefeated += enemyPointTemplate;
	roomFinished = true;
    totalRoomsFinished++;
	//spawnPowerUp(Health, 3);
	//Dynamic Adjustment Vars
	if (playerOne.getDamageTaken() == 0)
		roomsWithoutDamage++;
	else
        roomsWithoutDamage = 0;
    
    dynamicAdjust();
    
    playerOne.takeDamage(-playerOne.getDamageTaken());
}

void gameMaster::updateEnemies(){
    std::vector<int> toRemove;
    int enemiesDead = 0;
    for (int i = 0; i < enemies.size(); i++) {
        if (enemies[i]->isAlive()) {
            enemies[i]->behavior(&playerOne);
            
            for (int j = 0; j < enemies.size(); j++) {
                while (j != i && enemies[j]->isAlive() && entityCollison(enemies[i]->getMe(), enemies[j]->getMe())){
                    enemies[i]->move(Up);
                    enemies[j]->move(Down);
                }
            }
            if(!playerOne.isInvincible() && entityCollison(enemies[i]->getMe(), playerOne.getMe())){
                playerOne.adjustHealth(-1);
                
                playerOne.nowInvincible();
                playerOne.takeDamage(1);
            }
        }
        else if(!enemies[i]->isAddedToTotalDead()){
            enemiesDead++;
            enemiesDefeated+=1;
            enemies[i]->setCounted();
        }
        else{
            enemiesDead++;
        }
    }
        if(enemies.size() == enemiesDead && roomFinished != true){
            enemies.clear();
            finishRoom();
        }
    
}


void gameMaster::generateEnemies(){
	//Okay, so we need to consider how I want DDA to work
	//Ideally I want the players and the enemies to be "equal"
	//This means some important things, first the enemies should never have "unfair" advantages, so enemy speed should never be more than 2*playerSpeed
	//Let's generate a point template
	/*
		To generate elasticity (ie don't adjust unless player is breezing through or struggling, we'll have a threshold for creating a new point template
		That threshold will work like this: if the player took dmg, reasses, or if the player's stats changed reasses
	*/

	




    //Enemies generated currently is 2 + roomsWithoutDamage / 2
    //Enemy health increase when not taking damage, decreases when you do
    if(pointsDefeated < pointsToSpawnBoss){
        
        //Chase enemies are worth 30 points at their default
        //gunEnemies are worth 40~50ish
        //increasing speed on chaseEnemies has more point cost than increasing speed on gunEnemies
        
        /*
            From Experiencing it myself and listening to others more than 4 enemies gets very tricky, so I'm
            going to go ahead and limit the game such that it generates no more than 5 enemies ever
            As such from 80 up to 120 it's 2 always 
            from        120 up to 160 it's 2 - 3
            from        160 up to 200 it's 2 - 4
            from        240 up to bla it's 3 - 5
         cap at 300
         */
        int numberOfEnemiesToGenerate;
        //For 100 points generate 2 or 3 enemies for >150 generate 2 to 4  >200 3 to 5 >250 3 to 6 >300 4 to 7 highest mininimum is 6
        if(enemyPointTemplate < 120)
            numberOfEnemiesToGenerate = 2;
        else if (enemyPointTemplate < 160)
            numberOfEnemiesToGenerate = rand() % 2 + 2;
        else if (enemyPointTemplate < 200)
            numberOfEnemiesToGenerate = rand() % 3 + 2;
        else
            numberOfEnemiesToGenerate = rand() % 3 + 3;
        
        int pointsPerEnemy = enemyPointTemplate / numberOfEnemiesToGenerate;
        
        // range doesn't change
        int shotSpeed = 3, range = viewWidth;
        int enemyHealth = 3, enemySpeed = 1, enemyType, shotDmg = 1, enemySize = viewWidth / 40, shotSize = 25;
        float shotDelay = 1.2;
        int maxGunEnemies = 2, gunEnemiesSpawned = 0;
        int pointsUsed = 0;
        std::shared_ptr<enemy> placeholder;

        for (int i = 0; i < numberOfEnemiesToGenerate; i++) {
            enemyType = rand() % 2;
            
            if(pointsPerEnemy <= 40){
                //Generate a default enemy of type
                if (enemyType == 0 || gunEnemiesSpawned >= maxGunEnemies) {
                    enemyType = 0;
                    //Generate Chaser
                    vertex loc = generateEnemyLoc(enemySize);
                    placeholder = std::make_shared<chasingEnemy>(loc.getX(), loc.getY(), enemySize, enemySize, EnemyColor, enemyHealth);
                    enemies.push_back(placeholder);
                    pointsUsed = 40;
                    
                }
                else{
                    enemyHealth = 2;
                    //Generate Shooter
                    vertex loc = generateEnemyLoc(enemySize);
                    placeholder = std::make_shared<gunEnemy>(loc.getX(), loc.getY(), enemySize, enemySize, EnemyColor, enemyHealth, "primary", shotSpeed, range, shotSize, shotDelay / GAME_TIME_INTERVAL, shotDmg, &playerOne);
                    enemies.push_back(placeholder);
                    gunEnemiesSpawned++;
                    pointsUsed = 40;
                    
                }
            }
            else{
                int pointsToSpend = pointsPerEnemy - 40;
                if (pointsToSpend >= 30) {
                    enemyType = 1;
                }
                int upgrade;
                if(enemyType == 0 || gunEnemiesSpawned >= maxGunEnemies){
                    enemyType = 0;
                    while(pointsToSpend > 0 && enemySpeed < playerOne.getSpeed() - 2){
                        enemySpeed++;
                        pointsToSpend -= 30;
                        pointsUsed += 30;
                    }
                    while(pointsToSpend > 0){
                        enemyHealth++;
                        pointsToSpend -= 10;
                        pointsUsed += 10;
                    }
                    vertex loc = generateEnemyLoc(enemySize);
                    placeholder = std::make_shared<chasingEnemy>(loc.getX(), loc.getY(), enemySize, enemySize, EnemyColor, enemyHealth);
                    enemies.push_back(placeholder);
                    
                }
                else{
                    enemyHealth = 2; // Compensate for advantages of range
                    pointsToSpend -= 20; // Compensate for strength of gunEnemies
                    pointsUsed += 20;
                    
                    //While I would return to a system like the following if I were to make the game as a game
                    //      given that the focus of the game is on running tests, it's probably best to make more consistent the changes
                    /*while(pointsToSpend > 50){ // 40 point upgrades
                        //40 Point upgrades are: 1 dmg up shotSpeed - 1 shotSize * 1.5, 4 shotDelay - .1, 2 nada
                        upgrade = rand() % 7; // 0 to 6,
                        if (upgrade == 0) {
                            shotSpeed += 1;
                            shotSize *= 1.5;
                            pointsToSpend -= 40;
                            pointsUsed += 40;
                        }
                        else if(upgrade <= 4 && shotDelay > 0.25){
                            shotDelay -= 0.1;
                            pointsToSpend -= 40;
                            pointsUsed += 40;
                        }
                        else
                            break;
                    }
                    while(pointsToSpend > 25){ // 20 point upgrades
                        //20 point upgrades are: 2shotDelay - 0.05, 2health += 3 (size *= 1.5), 2enemySpeed += 1, 1nada
                        upgrade = rand() % 7;
                        if(upgrade <= 1 && shotDelay > 0.25){
                            shotDelay -= 0.05;
                            pointsToSpend -= 20;
                            pointsUsed += 20;
                        }
                        else if(upgrade <= 3){
                            enemyHealth += 3;
                            enemySize *= 1.5;
                            pointsToSpend -= 20;
                            pointsUsed += 20;
                        }
                        else if(upgrade <= 5){
                            shotDelay += 0.1;
                            shotSize += 10;
                            pointsToSpend -= 20;
                            pointsUsed += 20;
                        }
                        else
                            break;
                        
                    }
                    while(pointsToSpend > 0){ // 10 point upgrades, no defaults
                        //No default powers here, upgrade until out of points.
                        // health += 1 (size *= 1.1), shotSize += 3, shotDelay -= 0.05 shotSize += 5, repeat
                        upgrade = rand() % 5;
                        if (upgrade <= 1) {
                            enemyHealth += 1;
                            enemySize *= 1.1;
                            pointsToSpend -= 10;
                            pointsUsed += 10;
                        }
                        else if(upgrade <= 3){
                            shotSize += 3;
                            pointsToSpend -= 10;
                            pointsUsed += 10;
                        }
                        else{
                            enemySpeed += 1;
                            pointsToSpend -= 10;
                            pointsUsed += 10;
                        }
                    }*/
                    while(pointsToSpend > 5){//It's okay to 'borrow' 5 points, but no more than five
                        //Powers are:
                        //  Increased Health and Increased Size     45% (0, 1, 2, 3, 4, 5, 6, 7, 8)
                        //  Increased Delay, Increased Shot Size    25% (12, 13, 14, 15, 16)
                        //  Decreased Speed, +2 Health              15% (17, 18, 19)
                        upgrade = rand() % 20;
                        if(upgrade <= 10){
                            enemyHealth += 1;
                            enemySize *= 1.175;
                            pointsToSpend -= 10;
                            pointsUsed += 10;
                        }
                        else if(upgrade <= 15){
                            shotDelay += .10;
                            shotSize += 10;
                            pointsToSpend -= 10;
                            pointsUsed += 10;
                        }
                        else{
                            if(shotDelay > 0.5){
                                shotDelay -= 0.05;
                                pointsToSpend -= 10;
                                pointsUsed += 10;
                            }
                        }
                        
                    }
                    vertex loc = generateEnemyLoc(enemySize);
                    placeholder = std::make_shared<gunEnemy>(loc.getX(), loc.getY(), enemySize, enemySize, EnemyColor, enemyHealth, "primary", shotSpeed, range, shotSize, shotDelay / GAME_TIME_INTERVAL, shotDmg, &playerOne);
                    enemies.push_back(placeholder);
                    gunEnemiesSpawned++;
                    
                }
            }
            
            enemies[i]->adjustSpeed(enemySpeed);
            addEnemyData(placeholder, pointsUsed, enemyType);
            
            //Reset Defaults
            enemyHealth = 3; enemySpeed = 1; shotDmg = 1; enemySize = viewWidth / 40; shotSize = 25; shotDelay = 0.85; pointsUsed = 0;
        }
    }
    else{
        
        bossSpawned = true;
        enemies.push_back(std::make_shared<boss>(generateEnemyX(80), generateEnemyY(80), 110, 110, 255, 0, 50, 10, "Gun", 4, viewWidth, 30, 1.25 / GAME_TIME_INTERVAL, 1));
        //boss(<#int x#>, <#int y#>, <#int h#>, <#int w#>, <#defaultColors c#>, <#int hp#>, <#std::string n#>, <#int sS#>, <#int ran#>, <#int s#>, <#int sD#>, <#int dmg#>)
       
        
        enemies[0]->adjustSpeed(1);
    }
}

int gameMaster::roomAtIndex(int xP, int yP){
    //printf("Looking for room at: %d, %d", xP, yP);
    for (int i = 0; i < floor.size(); i++) {

        if (floor[i].atIndex(xP, yP)) {
            //printf("\tFOUND\n");
            return i;
        }
    }
    //printf("\n");
    return -1;
}

void gameMaster::resetPlayerPosition(){
    //viewWidth / 2 - 10, viewHeight / 2 - 10, 20, 20
    room curr = floor[currRoom];
    playerOne.resetVerts((curr.getMe().getStart().getX() + curr.getMe().getEnd().getX())/ 2 - 40, (curr.getMe().getStart().getY() + curr.getMe().getEnd().getY())/ 2 - 40, 80, 80);

}

bool gameMaster::entityCollison( entity one, entity two){
    if(one.getStart().getX() < two.getEnd().getX() &&
       one.getEnd().getX() > two.getStart().getX() &&
       one.getStart().getY() < two.getEnd().getY() &&
       one.getEnd().getY() > two.getStart().getY())
        return true;
    return false;
}

void gameMaster::resetOpenGLMatrices() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, viewWidth, 0, viewHeight, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void gameMaster::generateNewRoom() {
    updateEnemyPointTemplate();
	generateEnemies();
	roomFinished = false;
}

void gameMaster::applyPowerUp(powerUp plus) {
	//Damage, Range, ShotSpeed, FireRate, Health, ShotSize, MovementSpeed
	switch (plus.getType()) {
	case Damage:
		weapons[0].adjustDmg(plus.getStrength());
		break;
	case Range:
		weapons[0].adjustRange(plus.getStrength());
		break;
	case ShotSpeed:
		weapons[0].adjustShotSpeed(plus.getStrength());
		break;
	case FireRate:
		weapons[0].adjustFireRate(-plus.getStrength());
		break;
	case Health:
		playerOne.adjustHealth(plus.getStrength());
		break;
	case ShotSize:
		weapons[0].adjustShotSize(plus.getStrength());
		break;
	case MovementSpeed:
		playerOne.adjustSpeed(plus.getStrength());
		break;
	default:
		break;
	}
}

void gameMaster::updatePowerUps() {
	if (powerUpSpawned) {
		if (entityCollison(playerOne.getMe(), generalUp->getMe())) {
			applyPowerUp((*generalUp));
			delete generalUp;
			powerUpSpawned = false;
		}
	}
}

void gameMaster::spawnPowerUp(powerUpType t, int s) {
	int x, y, w, h;
	if (playerOne.getMe().getStart().getX() < viewWidth / 2) {
		x = viewWidth * 0.75;
		w = viewWidth / 20;
	}
	else {
		x = viewWidth * 0.25;
		w = viewWidth / 20;
	}

	if (playerOne.getMe().getStart().getY() < viewHeight / 2) {
		y = viewHeight * 0.75;
		h = viewHeight / 20;
	}
	else {
		y = viewHeight * 0.25;
		h = viewHeight / 20;
	}
	generalUp = new powerUp(t, s, x, y, w, h);


	powerUpSpawned = true;
}

/*
    Rules are:
        If a player takes damage in two consecutive rooms, spawn a nonHP powerup and DON'T adjust points
        Whenever a player completes 4 rooms give them 1 heart
 
        If a player takes more than two damage on a room, immediatly adjust
        If the player completes 3 rooms without taking damage, adjust
 
 
        Let's start by trying an adjust everyRoom
        Whenever a player completes 4 rooms, they get 1 hp,
        Every 10 Enemies they get a random powerup that isn't health?
 
 */
bool gameMaster::dynamicAdjust() {
    
    //int playerStrength = roomsWithoutDamage * weapons[0].getDmg() + roomsWithoutDamage * playerOne.getSpeed();
    /*if (roomsWithoutDamage < 3)
        enemyPointTemplate = (weapons[0].getDmg() + playerOne.getSpeed() + playerOne.getHealth()) * 10;
    else
        enemyPointTemplate = roomsWithoutDamage / 3 * (weapons[0].getDmg() + playerOne.getSpeed() + playerOne.getHealth()) * 10;
    */
    if(DYNAMIC){
        if(roomsWithoutDamage >= 1)
            enemyPointTemplate += int((roomsWithoutDamage + 1) / 2.0) * 10;
        else if(playerOne.getDamageTaken() < 2)
            enemyPointTemplate -= 10;
        else
            enemyPointTemplate *= 0.70;
        
        if(enemyPointTemplate > 300){
            enemyPointTemplate = 300;
        }
        
        /*if(pointsDefeated >= pointsToPowerUp){
            int pType = rand() % 2;
            if(pType == 0){
                spawnPowerUp(FireRate, 0.05 / GAME_TIME_INTERVAL);
            }
            else if(pType == 1){
                spawnPowerUp(ShotSize, 10);
            }
             else{
             spawnPowerUp(Range, 100);
             }
            pointsToPowerUp += 500;
        }*/

    }
    else{
        if (enemyPointTemplate < 300) {
            enemyPointTemplate += 20;
        }
        else{
            enemyPointTemplate = 300;
        }
        //enemyPointTemplate += 15;
    }
    
	return true;
}

int gameMaster::generateEnemyX(int enemySize){
   // int leftOrRight = rand() % 2; //0 is left 1 is right
    
    int enemyX;
    if(playerOne.getCenterX() >= viewWidth / 2){
        enemyX = (rand() % ((viewWidth / 2 - (enemySize * 2)) -  floor[currRoom].getMe().getStart().getX() + enemySize)) + floor[currRoom].getMe().getStart().getX() + enemySize;
    }
    else{
        enemyX = (rand() % ((floor[currRoom].getMe().getEnd().getX() - enemySize) - ((viewWidth / 2) + (enemySize * 2)))) + (viewWidth / 2) + (enemySize * 2);
    }
    return enemyX;
}
int gameMaster::generateEnemyY(int enemySize){
    int enemyY;
    //Choose Left, right top or bottom
    
    
    if(playerOne.getCenterY() >= viewHeight / 2){
        enemyY = (rand() % (viewHeight / 2 - enemySize * 2 - floor[currRoom].getMe().getStart().getY() + enemySize)) + floor[currRoom].getMe().getStart().getY() + enemySize;
    }
    else{
        enemyY = (rand() % (floor[currRoom].getMe().getEnd().getY() - enemySize - (viewHeight / 2 + enemySize * 2))) + (viewHeight / 2 + enemySize * 2);
    }
    return enemyY;
}

vertex gameMaster::generateEnemyLoc(int enemySize){
    int loc = rand() % 4; //Left right top bot
    int eY, eX;
    if (loc == 0) {
        eY = rand() % viewHeight;
        eX = -enemySize;
    }
    else if (loc == 1){
        eY = rand() % viewHeight;
        eX = viewWidth + enemySize;
    }
    else if (loc == 2){
        eY = -enemySize;
        eX = rand() % viewWidth;
    }
    else{
        eY = viewHeight + enemySize;
        eX = rand() % viewWidth;
    }
    
    return vertex(eX, eY);
    
}

void gameMaster::updatePlayerData(){
    /*
     <player>
     *health*
     *totalDamageTaken*
     </player>
     */
    std::string s = "<player>\n";
    
    s.append(std::to_string(playerOne.getHealth()));
    s.append("\n");
    s.append(std::to_string(playerOne.getDamageTaken()));
    s.append("\n");
    
    s.append("</player>\n");
    
    
    testData.pushBack(s);
    
}

void gameMaster::updateEnemyPointTemplate(){
    std::string s = "<enemyPointTemplate>\n";
    s.append(std::to_string(enemyPointTemplate));
    s.append("\n");
    s.append("</enemyPointTemplate>\n");
    
    testData.pushBack(s);
}

void gameMaster::addEnemyData(std::shared_ptr<enemy> e, int pointVal, int type){
    std::string s;
    
    if (type == 0) {//chase enemy
        s = "<ChaseEnemy>\n";
        s.append(std::to_string(e->getHealth()));
        s.append("\n");
        s.append(std::to_string(e->getSpeed()));
        s.append("\n");
        s.append(std::to_string(pointVal));
        s.append("\n");
        s.append("</ChaseEnemy>\n");
    }
    else{//gun enemy
        s = "<GunEnemy>\n";
        s.append(std::to_string(e->getHealth()));
        s.append("\n");
        s.append(std::to_string(e->getSpeed()));
        s.append("\n");
        
        weapon* w = e->getWeapon();
        
        s.append(std::to_string(w->getDmg()));
        s.append("\n");
        s.append(std::to_string(w->getRange()));
        s.append("\n");
        s.append(std::to_string(w->getShotSize()));
        s.append("\n");
        s.append(std::to_string(w->getShotDelay()));
        s.append("\n");
        s.append(std::to_string(pointVal));
        s.append("\n");
        s.append("</GunEnemy>\n");
    }
    
    testData.pushBack(s);
}
