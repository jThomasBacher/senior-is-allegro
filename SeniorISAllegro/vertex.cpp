//
//  vertex.cpp
//  seniorIS
//
//  Created by John Bacher on 9/26/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "vertex.hpp"

vertex::vertex(int x, int y) : myColor(Red){
    setX(x);
    setY(y);
}
vertex::vertex(int x, int y, defaultColors c) : myColor(c){
    setY(y);
    setX(x);
}
vertex::vertex(int x, int y, int r, int g, int b) : myColor(r, g, b){
    setX(x);
    setY(y);
}

void vertex::createVertex(bool changeColor){
    if (changeColor)
        myColor.setGLColor();
    glVertex2i(x, y);
}
