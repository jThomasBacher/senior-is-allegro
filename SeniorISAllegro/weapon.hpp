//
//  weapon.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 11/2/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef weapon_hpp
#define weapon_hpp

#include "projectile.hpp"
#include <string>
#include <vector>

class weapon {
    std::string name;
    int shotSpeed;
    int range;
    int size;
	int shotDelay; //shotDelay * 0.25 = seconds between shots
    int damage;
    
	//ALLEGRO_TIMER *shotTimer;
    
    unsigned long startTime;
    
    std::vector<projectile> shots;
    
public:
    weapon(std::string n, int sS, int ran, int s, int sD, int dmg);
    
    void deleteProjectile(int i);
    
    //Generates a new projectile if shotTimer > shotDelay and then resets shotTimer, else nothing happens
    void fire(entity user, direction heading, int r, int g, int b);
    void fire(entity user, vertex target, int r, int g, int b);
    
    //Moves all projectiles, removes projectiles from vector that are done
    void update();
    
    std::vector<projectile> getShots(){return shots;}
    int getDmg(){return damage;}
    int getRange(){return range;}
    int getShotSize(){return size;}
    float getShotDelay(){return shotDelay * GAME_TIME_INTERVAL;}

	void adjustDmg(int delta) { damage += delta; }
	void adjustShotSpeed(int delta) { shotSpeed += delta; }
	void adjustRange(int delta) { range += delta; }
	void adjustShotSize(int delta) { size += delta; }
	void adjustFireRate(int delta) { shotDelay += delta; }
    
    void halfWindWeapon(){startTime += (shotDelay / 2);}
};

#endif /* weapon_hpp */
