//
//  gunEnemy.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 1/13/18.
//  Copyright © 2018 Tommy Bacher. All rights reserved.
//

#ifndef gunEnemy_hpp
#define gunEnemy_hpp

#include "enemy.hpp"

class gunEnemy : public enemy {
    weapon primaryWeapon;
    
    vertex targetLoc;
    
    unsigned long startTime, timeUntilMove;
    
    direction firingDirection;
    
    void chooseNewLoc(player *target);
public:
    
    gunEnemy(int x, int y, int h, int w, defaultColors c, int hp, std::string n, int sS, int ran, int s, int sD, int dmg, player *target);
    gunEnemy(vertex start, vertex e, defaultColors c,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg, player *target);
    gunEnemy(int x, int y, int h, int w, int r, int g, int b,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg, player *target);
    gunEnemy(vertex start, vertex e, int r, int g, int b,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg, player *target);
    
    void draw();
    
    void behavior(player *target);
    
    weapon* getWeapon(){return &primaryWeapon;}
    
};

#endif /* gunEnemy_hpp */
