//
//  defines.h
//  SeniorISAllegro
//
//  Created by John Bacher on 10/10/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef allegro_includes
#define allegro_includes

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_opengl.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>


#endif

#ifndef gl_includes
#define gl_includes

#include <stdio.h>
#include <math.h>
#ifdef __APPLE__
#include <OpenGL/glu.h>
#include <OpenGL/glext.h>
#elif  _WIN32 || _WIN64 //Need to fix this, as it assumes windows
#include <Windows.h>
#include <GL/GL.h>
#include <gl/GLU.h>
#include <memory>
//#define glXGetProcAddress(x) (*glXGetProcAddressARB)((const GLubyte*)x)
#endif

#endif

#ifndef defines_h
#define defines_h

#define DYNAMIC 1

#define SECS_TO_USECS(x)	(1000000.0 / (x))
//ALL times must occur in increments of this interval, the closer it gets to zero the more precision
// that can occur in times, but the more additions the program must do (if I understand how allegro timers work properly
#define GAME_TIME_INTERVAL 0.05

enum KEYS{keyW, keyA, keyS, keyD, keySpace, keyEsc, keyLeft, keyRight, keyUp, keyDown};
enum direction {Up, Left, Right, Down, Undefined};

extern ALLEGRO_TIMER *gameTimer;
extern int winWidth;
extern int winHeight;
extern int viewWidth;
extern int viewHeight;
extern int xPortalOffset;
extern int yPortalOffset;
extern int xPortalSize;
extern int yPortalSize;
extern int machineType;
extern float FPS;
extern float speedFactor; //All speeds are to be multiplied by this number
							//Need to figure out how to properly get it such that if feels like it's running at the same speed on two seperate machines no matter what

void reduceFraction (int &numerator, int &denominator);

//Greatest Common Denom using Euclid's Algorithm
int gcd(int u, int v);

#endif /* defines_h */
