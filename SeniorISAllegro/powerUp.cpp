#include "powerUp.hpp"



powerUp::powerUp(powerUpType t, int s, int x, int y, int w, int h) : me(x, y, w, h, Yellow)
{
	type = t;
	strength = s;
}


void powerUp::drawPowerUp() {
	glColor3ub(255, 255, 0);

	//When I actually implemented this, the first two seemed to provide a good enough star

	glBegin(GL_TRIANGLES);
	//1
	glVertex2i(me.getCenterX(), me.getEnd().getY());
	glVertex2i(me.getCenterX() + me.getWidth() / 2, me.getCenterY());
	glVertex2i(me.getStart().getX(), me.getStart().getY());

	//2
	glVertex2i(me.getCenterX(), me.getEnd().getY());
	glVertex2i(me.getEnd().getX(), me.getStart().getY());
	glVertex2i(me.getCenterX() - me.getWidth() / 2, me.getCenterY());

	glEnd();
}
