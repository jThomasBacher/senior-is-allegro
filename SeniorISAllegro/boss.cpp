//
//  boss.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 11/29/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "boss.hpp"

boss::boss(int x, int y, int h, int w, defaultColors c, int hp, std::string n, int sS, int ran, int s, int sD, int dmg) :  enemy(x, y, h, w, c, hp), northGun(n, sS, ran, s, sD, dmg), southGun(n, sS, ran, s, sD, dmg), westGun(n, sS, ran, s, sD, dmg), eastGun(n, sS, ran, s, sD, dmg), targetLoc(x + (h / 2), y + (w / 2))
{
   
    //Current constraints: move for at least 1.5 seconds, at most 3 seconds
    timeToMove = rand() % int(1.5 / GAME_TIME_INTERVAL) + int(1.5 / GAME_TIME_INTERVAL); // tTM * 0.25 = seconds to move and stay at loc
    startTime = al_get_timer_count(gameTimer);
    southGun.halfWindWeapon();
    northGun.halfWindWeapon();
    
    //startTime = 0, timeToMove = 16, when should we stop moving?
    // When gameTimer >= 16 what about sT = 1 tTM = 16? when gameTimer >= 17
    
}

boss::boss(vertex start, vertex e, defaultColors c,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg) : enemy(start, e, c, hp), northGun(n, sS, ran, s, sD, dmg), southGun(n, sS, ran, s, sD, dmg), westGun(n, sS, ran, s, sD, dmg), eastGun(n, sS, ran, s, sD, dmg), targetLoc((start.getX() + e.getX()) / 2, (start.getY() + e.getY()) / 2)
{
    
    //Current constraints: move for at least 3.5 seconds, at most 7 seconds
    timeToMove = rand() % int(1.5 / GAME_TIME_INTERVAL) + int(1.5 / GAME_TIME_INTERVAL); // tTM * 0.25 = seconds to move and stay at loc
    startTime = al_get_timer_count(gameTimer);
    southGun.halfWindWeapon();
    northGun.halfWindWeapon();
}

boss::boss(int x, int y, int h, int w, int r, int g, int b,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg) : enemy(x, y, h, w, r, g, b, hp), northGun(n, sS, ran, s, sD, dmg), southGun(n, sS, ran, s, sD, dmg), westGun(n, sS, ran, s, sD, dmg), eastGun(n, sS, ran, s, sD, dmg), targetLoc(x + (h / 2), y + (w / 2))
{
    
    //Current constraints: move for at least 3.5 seconds, at most 7 seconds
    timeToMove = rand() % int(1.5 / GAME_TIME_INTERVAL) + int(1.5 / GAME_TIME_INTERVAL); // tTM * 0.25 = seconds to move and stay at loc
    startTime = al_get_timer_count(gameTimer);
    southGun.halfWindWeapon();
    northGun.halfWindWeapon();
}

boss::boss(vertex start, vertex e, int r, int g, int b,  int hp, std::string n, int sS, int ran, int s, int sD, int dmg) : enemy(start, e, r, g, b, hp), northGun(n, sS, ran, s, sD, dmg), southGun(n, sS, ran, s, sD, dmg), westGun(n, sS, ran, s, sD, dmg), eastGun(n, sS, ran, s, sD, dmg), targetLoc((start.getX() + e.getX()) / 2, (start.getY() + e.getY()) / 2)
{
    
    //Current constraints: move for at least 3.5 seconds, at most 7 seconds
   timeToMove = rand() % int(1.5 / GAME_TIME_INTERVAL) + int(1.5 / GAME_TIME_INTERVAL); // tTM * 0.25 = seconds to move and stay at loc
    startTime = al_get_timer_count(gameTimer);
    southGun.halfWindWeapon();
    northGun.halfWindWeapon();
}

void boss::behavior(player *target){
    northGun.fire(this->getMe(), Up, 200, 140, 50);
    southGun.fire(this->getMe(), Down, 200, 140, 50);
    westGun.fire(this->getMe(), Left, 200, 140, 50);
    eastGun.fire(this->getMe(), Right, 200, 140, 50);
    
    int toRemove = -10;
    northGun.update();
    for (int i = 0; i < northGun.getShots().size(); i++) {
        if(!target->isInvincible() && entityCollison(northGun.getShots()[i].getMe(), target->getMe())){
            target->adjustHealth(-northGun.getDmg());
            target->nowInvincible();
            target->takeDamage(northGun.getDmg());
            toRemove = i;
        }
    }
    if (toRemove != -10)
        northGun.deleteProjectile(toRemove);
    toRemove = -10;
    
    southGun.update();
    for (int i = 0; i < southGun.getShots().size(); i++) {
        if(!target->isInvincible() && entityCollison(southGun.getShots()[i].getMe(), target->getMe())){
            target->adjustHealth(-southGun.getDmg());
            target->nowInvincible();
            target->takeDamage(southGun.getDmg());
            toRemove = i;
        }
    }
    if (toRemove != -10)
        southGun.deleteProjectile(toRemove);
    toRemove = -10;
    
    westGun.update();
    for (int i = 0; i < westGun.getShots().size(); i++) {
        if(!target->isInvincible() && entityCollison(westGun.getShots()[i].getMe(), target->getMe())){
            target->adjustHealth(-westGun.getDmg());
            target->nowInvincible();
            target->takeDamage(westGun.getDmg());
            toRemove = i;
        }
    }
    if (toRemove != -10)
        westGun.deleteProjectile(toRemove);
    toRemove = -10;
    
    eastGun.update();
    for (int i = 0; i < eastGun.getShots().size(); i++) {
        if(!target->isInvincible() && entityCollison(eastGun.getShots()[i].getMe(), target->getMe())){
            target->adjustHealth(-eastGun.getDmg());
            target->nowInvincible();
            target->takeDamage(eastGun.getDmg());
            toRemove = i;
        }
    }
    if(toRemove != -10)
        eastGun.deleteProjectile(toRemove);
    toRemove = -10;
    
    
    if(!target->isInvincible() && entityCollison(getMe(), target->getMe())){
        target->adjustHealth(-1);
        target->nowInvincible();
        target->takeDamage(1);
        //damageTaken++;
    }
    
    if ((getCenterX() == targetLoc.getX() && getCenterY() == targetLoc.getY()) || al_get_timer_count(gameTimer) > startTime + timeToMove ) {
        
        int xLowerBound = getMe().getWidth(), yLowerBound = getMe().getHeight();
        int xUpperBound = viewWidth - xLowerBound, yUpperBound = viewHeight - yLowerBound;
        
        targetLoc.setX( (rand() % (xUpperBound - xLowerBound)) + xLowerBound);
        targetLoc.setY( (rand() % (yUpperBound - yLowerBound)) + yLowerBound);
        
        startTime = al_get_timer_count(gameTimer);
        timeToMove = rand() % int(1.5 / GAME_TIME_INTERVAL) + int(1.5 / GAME_TIME_INTERVAL);
    }
    
    direction d1 = Undefined, d2;
    bool notLeftRight = false;
    if(targetLoc.getX() - getSpeed() - 1 > getCenterX())
        d1 = Right;
    else if(targetLoc.getX() + getSpeed() + 1 < getCenterX())
        d1 = Left;
    else
        notLeftRight = true;
    
    
    
    if (targetLoc.getY() - getSpeed() - 1 > getCenterY())
        d2 = Up;
    else if(targetLoc.getY() + getSpeed() + 1 < getCenterY())
        d2 = Down;
    else
        d2 = d1;
    
    if (notLeftRight) {
        d1 = d2;
    }
    
    move(d1);
    move(d2);
}

void boss::draw(){
    enemy::draw();
    for (int i = 0; i < northGun.getShots().size(); i++) {
        northGun.getShots()[i].draw();
    }
    for (int i = 0; i < southGun.getShots().size(); i++) {
        southGun.getShots()[i].draw();
    }
    for (int i = 0; i < westGun.getShots().size(); i++) {
        westGun.getShots()[i].draw();
    }
    for (int i = 0; i < eastGun.getShots().size(); i++) {
        eastGun.getShots()[i].draw();
    }
}

/*bool boss::entityCollison( entity one, entity two){
    if(one.getStart().getX() < two.getEnd().getX() &&
       one.getEnd().getX() > two.getStart().getX() &&
       one.getStart().getY() < two.getEnd().getY() &&
       one.getEnd().getY() > two.getStart().getY())
        return true;
    return false;
}*/
