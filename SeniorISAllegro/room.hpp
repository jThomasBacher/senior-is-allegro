//
//  room.hpp
//  SeniorISAllegro
//
//  Created by John Bacher on 10/4/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef room_hpp
#define room_hpp

#include "entity.hpp"

class room {
    entity me;
    entity *leftDoor, *rightDoor, *topDoor, *bottomDoor;
    
    int xPos, yPos; // These Positions are within the floor vertex, so the starting room is xPos 0, yPos 0 and up is +1, down is -1 right is +1 left is -1 etc
    
    bool complete; //If a room isn't complete, must fight it, otherwise just show exits
    
    /*
        -1 defines a null connection
        >=0 defines the position of the room
        -2 defines an impossible connection
    */
    int leftExit, rightExit, topExit, bottomExit;
    
public:
    room(int x, int y, int w, int h, defaultColors c, int xP, int yP);
    room(vertex s, vertex e, defaultColors c, int xP, int yP);
    room(int x, int y, int w, int h, int r, int g, int b, int xP, int yP);
    room(vertex s, vertex e, int r, int g, int b, int xP, int yP);
    
    ~room();
    
    void drawRoom(); // Draws this specific room, color will be this room's backColor
    
    entity getMe(){return me;}
    
    bool isComplete(){return complete;}
    
    void finish(){complete = true;}
    
    bool atIndex(int xP, int yP){return (xP == xPos && yP == yPos);}
    
    int getLeftExit(){return leftExit;}
    int getRightExit(){return rightExit;}
    int getTopExit(){return topExit;}
    int getBottomExit(){return bottomExit;}
    
    entity getLeftDoor(){return *leftDoor;}
    entity getRightDoor(){return *rightDoor;}
    entity getTopDoor(){return *topDoor;}
    entity getBottomDoor(){return *bottomDoor;}
    
    void setLeftExit(int i){leftExit = i;}
    void setRightExit(int i){rightExit = i;}
    void setTopExit(int i){topExit = i;}
    void setBottomExit(int i){bottomExit = i;}
    
    int getXPos(){return xPos;}
    int getYPos(){return yPos;}
    
    void instantiateTopDoor();
    void instantiateBottomDoor();
    void instantiateLeftDoor();
    void instantiateRightDoor();
};

#endif /* room_hpp */
