//
//  projectile.cpp
//  SeniorISAllegro
//
//  Created by John Bacher on 11/2/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#include "projectile.hpp"

projectile::projectile(int x, int y, int h, int w, defaultColors c, int rise, int run, int sp, bool l, bool rt) : me(x, y, h, w, c){
    distTraveled = 0;
    this->rise = rise;
    this->run = run;
    speed = sp * speedFactor;
    
    left = l;
    up = rt;
    
    if(run != 0)
         riseOverRun = float(rise) / float(run);
    else
        riseOverRun = 1;
    
   
    if(signbit(riseOverRun) != 0){
        riseOverRun *= -1;
    }
   // printf("\nrOR:%f\trise:%d\trun:%d\n\n", riseOverRun, rise, run);
}
projectile::projectile(vertex s, vertex e, defaultColors c, int rise, int run, int sp, bool l, bool rt) : me(s, e, c){
    distTraveled = 0;
    this->rise = rise;
    this->run = run;
    speed = sp * speedFactor;
    
    left = l;
    up = rt;
    
    if(run != 0)
        riseOverRun = float(rise) / float(run);
    else
        riseOverRun = 1;
    if(signbit(riseOverRun) != 0){
        riseOverRun *= -1;
    }
    //printf("\nrOR:%f\trise:%d\trun:%d\n\n", riseOverRun, rise, run);
}
projectile::projectile(int x, int y, int h, int w, int r, int g, int b, int rise, int run, int sp, bool l, bool rt) : me(x, y, h, w, r, g, b){
    distTraveled = 0;
    this->rise = rise;
    this->run = run;
    speed = sp * speedFactor;
    
    left = l;
    up = rt;
    
    if(run != 0)
        riseOverRun = float(rise) / float(run);
    else
        riseOverRun = 1;
    if(signbit(riseOverRun) != 0){
        riseOverRun *= -1;
    }
    //printf("\nrOR:%f\trise:%d\trun:%d\n\n", riseOverRun, rise, run);
}
projectile::projectile(vertex s, vertex e, int r, int g, int b, int rise, int run, int sp, bool l, bool rt) : me(s, e, r, g, b){
    distTraveled = 0;
    this->rise = rise;
    this->run = run;
    speed = sp * speedFactor;
    
    left = l;
    up = rt;
    
    if(run != 0)
        riseOverRun = float(rise) / float(run);
    else
        riseOverRun = 1;
    if(signbit(riseOverRun) != 0){
        riseOverRun *= -1;
    }
    //printf("\nrOR:%f\trise:%d\trun:%d\n\n", riseOverRun, rise, run);
}


void projectile::update(){
    
    if(riseOverRun < 1){
        
        if(left){
            me.left(speed);
        }
        else{
            me.right(speed);
        }
        
        runTrav += riseOverRun;
        if(runTrav >= 1){
            if(up)
                me.up(speed);
            else
                me.down(speed);
            runTrav -= 1;
        }
    }
    else{
        if(up)
            me.up(speed);
        else
            me.down(speed);
        if(run != 0)
            riseTrav++;
        
        if(riseTrav >= riseOverRun){
            if(left){
                me.left(speed);
            }
            else{
                me.right(speed);
            }
            riseTrav = riseTrav - riseOverRun;
        }
    }
    
  /*  if(riseTrav >= rise && runTrav >= run){
        riseTrav = 0;
        runTrav = 0;
    }
    
    if(riseTrav < rise){
        me.up(speed);
        riseTrav++;
    }
    
    if(runTrav < run){
        me.right(speed);
        runTrav++;
    } */
    
    distTraveled += (speed);
}

void projectile::draw(){
    me.drawEntity();
}
