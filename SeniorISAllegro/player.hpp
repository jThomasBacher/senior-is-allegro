//
//  player.hpp
//  seniorIS
//
//  Created by John Bacher on 9/26/17.
//  Copyright © 2017 Tommy Bacher. All rights reserved.
//

#ifndef player_hpp
#define player_hpp


#include "entity.hpp"



class player {
    entity me;

    int speed;
    int health;

    bool invincible;
    
    unsigned long startTime, invinTimer = 3.0 / GAME_TIME_INTERVAL;
    int damageTaken;
    int totalDamageTaken; //isn't wiped after every room
	

public:
    player(int x, int y, int h, int w, defaultColors c);
    player(vertex s, vertex e, defaultColors c);
    player(int x, int y, int h, int w, int r, int g, int b);
    player(vertex s, vertex e, int r, int g, int b);

    void drawPlayer();
    void drawHealth();

    bool isInvincible(){return invincible;}
    void nowInvincible() { invincible = true; startTime = al_get_timer_count(gameTimer);}
	void removeInvincible() { invincible = false; }

    void move(direction d);

    void resetVerts(int x, int y, int w, int h);
    void resetVerts(vertex s, vertex e);

    entity getMe(){return me;}
    int getSpeed(){return speed;}
    int getHealth(){return health;}

    void adjustSpeed(int delta){speed += (delta * speedFactor);}
    void adjustHealth(int delta){health += delta;}

    int getCenterX(){return me.getCenterX();}
    int getCenterY(){return me.getCenterY();}
    
    void takeDamage(int i);
    int getDamageTaken(){return damageTaken;}
};

#endif /* player_hpp */
